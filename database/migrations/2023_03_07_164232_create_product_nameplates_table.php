<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nameplates', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('nameplate_id')->unique();

            $table->string('name');
            $table->string('code')->unique();

            $table->string('text_color');
            $table->string('background_color');

            $table->timestamps(6);
        });

        Schema::create('nameplate_product', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('nameplate_product_id')->unique();

            $table->unsignedBigInteger('nameplate_id');
            $table->unsignedBigInteger('product_id');

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nameplates');
        Schema::dropIfExists('nameplate_product');
    }
};
