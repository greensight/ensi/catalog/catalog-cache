<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Offer;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $product_id - id товара
 * @property bool $allow_publish  - Признак активности для витрины
 * @property int|null $price - Цена оффера
 * @property bool $is_active - Системная активность оффера
 * @property bool $is_real_active - Итоговая активность оффера, по которой оффер попадет на витрину
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class OfferPayload extends Payload
{
}
