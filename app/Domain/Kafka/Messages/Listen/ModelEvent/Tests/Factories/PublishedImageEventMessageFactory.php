<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class PublishedImageEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'product_id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->company,
            'sort' => $this->faker->randomNumber(),
            'file' => $this->faker->optional()->filePath(),
            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
