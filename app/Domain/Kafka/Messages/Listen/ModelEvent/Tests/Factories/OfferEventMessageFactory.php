<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class OfferEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'price' => $this->faker->nullable()->numberBetween(10, 100_000),
            'allow_publish' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
            'is_real_active' => $this->faker->boolean(),
            'active_comment' => $this->faker->nullable()->word(),
            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
