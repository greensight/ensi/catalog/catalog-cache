<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id Product ID from PIM
 *
 * @property string $name
 * @property string $code Code to use in URL
 * @property string|null $description
 * @property int|null $brand_id Brand ID from PIM
 * @property string|null $external_id
 * @property int $type Product type from ProductTypeEnum
 * @property int $status_id
 * @property string|null $status_comment
 * @property bool $allow_publish
 * @property string|null $barcode EAN
 * @property string $vendor_code Article
 * @property bool $is_adult Is product 18+
 * @property float|null $weight Net weight in kg
 * @property float|null $weight_gross Gross weight in kg
 * @property float|null $width Width in mm
 * @property float|null $height Height in mm
 * @property float|null $length Length in mm
 * @property string|null $main_image_file
 *
 * @property int|null $uom Unit of measurement
 * @property int|null $tariffing_volume Unit of tariffication
 * @property float|null $order_step
 * @property float|null $order_minvol Minimum quantity for order
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class PublishedProductPayload extends Payload
{
}
