<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Brand;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание бренда
 * @property bool $is_active Активность бренда
 * @property string|null $logo_file Файл логотипа
 * @property string|null $logo_url URL логотипа
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class BrandPayload extends Payload
{
}
