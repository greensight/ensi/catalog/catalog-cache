<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Nameplate;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\NameplateEventMessageFactory;

class NameplateEventMessage extends ModelEventMessage
{
    public NameplatePayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new NameplatePayload($attributes);
    }

    public static function factory(): NameplateEventMessageFactory
    {
        return NameplateEventMessageFactory::new();
    }
}
