<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Nameplate;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property string $name - наименование тега
 * @property string $code - код тега
 * @property string $background_color - цвет фона
 * @property string $text_color - цвет текста
 * @property bool $is_active - активность
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class NameplatePayload extends Payload
{
}
