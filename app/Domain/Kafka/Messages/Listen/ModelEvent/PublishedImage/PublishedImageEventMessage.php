<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedImage;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\PublishedImageEventMessageFactory;

class PublishedImageEventMessage extends ModelEventMessage
{
    public PublishedImagePayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new PublishedImagePayload($attributes);
    }

    public static function factory(): PublishedImageEventMessageFactory
    {
        return PublishedImageEventMessageFactory::new();
    }
}
