<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct\PublishedProductEventMessage;
use App\Domain\Offers\Models\Product;
use RdKafka\Message;

class ListenPublishedProductAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = PublishedProductEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        if (!$this->checkSync($eventMessage)) {
            return;
        };

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'allow_publish',
                'main_image_file',
                'brand_id',
                'name',
                'code',
                'description',
                'type',
                'vendor_code',
                'barcode',
                'weight',
                'weight_gross',
                'width',
                'height',
                'length',
                'is_adult',
                'uom',
                'tariffing_volume',
                'order_step',
                'order_minvol',
            ],
            findModel: fn () => Product::query()->where('product_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Product();
                $model->product_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (Product $model) use ($modelPayload) {
                $model->allow_publish = $modelPayload->allow_publish;
                $model->main_image_file = $modelPayload->main_image_file;
                $model->brand_id = $modelPayload->brand_id;
                $model->name = $modelPayload->name;
                $model->code = $modelPayload->code;
                $model->description = $modelPayload->description;
                $model->type = $modelPayload->type;
                $model->vendor_code = $modelPayload->vendor_code;
                $model->barcode = $modelPayload->barcode;
                $model->weight = $modelPayload->weight;
                $model->weight_gross = $modelPayload->weight_gross;
                $model->width = $modelPayload->width;
                $model->height = $modelPayload->height;
                $model->length = $modelPayload->length;
                $model->is_adult = $modelPayload->is_adult;
                $model->uom = $modelPayload->uom;
                $model->tariffing_volume = $modelPayload->tariffing_volume;
                $model->order_step = $modelPayload->order_step;
                $model->order_minvol = $modelPayload->order_minvol;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }

    private function checkSync(PublishedProductEventMessage $eventMessage): bool
    {
        if ($eventMessage->event == ModelEventMessage::CREATE
            && $eventMessage->attributes->allow_publish) {
            // Созданные только что неопубликованные товары нет смысла сохранять
            return true;
        }

        if ($eventMessage->event == ModelEventMessage::UPDATE
            && ($eventMessage->attributes->allow_publish || in_array('allow_publish', $eventMessage->dirty))) {
            // Сохраняем только изменения по опубликованным товарам, или у которых изменился статус публикации
            // Нет смысла сохранять изменение по неопубликованным товарам, Старое состояние неопубликованного товара на витрине не критично
            return true;
        }

        if ($eventMessage->event == ModelEventMessage::DELETE) {
            return true;
        }

        return false;
    }
}
