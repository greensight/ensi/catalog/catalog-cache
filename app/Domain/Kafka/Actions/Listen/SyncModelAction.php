<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use Illuminate\Database\Eloquent\Model;

class SyncModelAction
{
    public function execute(
        ModelEventMessage $eventMessage,
        array $interestingUpdatedFields,
        callable $findModel,
        callable $createModel,
        callable $fillModel,
        ?callable $isToDelete = null,
    ): void {
        $needCreate = $eventMessage->event == ModelEventMessage::CREATE;
        $needUpdate = $eventMessage->event == ModelEventMessage::UPDATE &&
            (!$interestingUpdatedFields || array_intersect($interestingUpdatedFields, $eventMessage->dirty ?? []));
        $needDelete = $eventMessage->event == ModelEventMessage::DELETE || ($isToDelete && $isToDelete());

        if ($needDelete) {
            $model = $findModel();
            $model?->delete();
        } elseif ($needCreate || $needUpdate) {
            /** @var Model|null $model */
            $model = $findModel();
            if (!$model) {
                $model = $createModel();
            }

            $fillModel($model);
            if ($model->isDirty()) {
                $model->save();
            }
        }
    }
}
