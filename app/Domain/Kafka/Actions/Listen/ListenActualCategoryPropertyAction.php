<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ActualCategoryProperty\ActualCategoryPropertyEventMessage;
use App\Domain\Offers\Models\ActualCategoryProperty;
use RdKafka\Message;

class ListenActualCategoryPropertyAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = ActualCategoryPropertyEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'property_id',
                'category_id',
                'is_gluing',
            ],
            findModel: fn () => ActualCategoryProperty::query()->where('actual_category_property_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new ActualCategoryProperty();
                $model->actual_category_property_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (ActualCategoryProperty $model) use ($modelPayload) {
                $model->property_id = $modelPayload->property_id;
                $model->category_id = $modelPayload->category_id;
                $model->is_gluing = $modelPayload->is_gluing;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
