<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Property\PropertyEventMessage;
use App\Domain\Offers\Models\Property;
use RdKafka\Message;

class ListenPropertyAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = PropertyEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'display_name',
                'code',
                'type',
                'is_public',
                'is_active',
            ],
            findModel: fn () => Property::query()->where('property_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Property();
                $model->property_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (Property $model) use ($modelPayload) {
                $model->name = $modelPayload->display_name;
                $model->code = $modelPayload->code;
                $model->type = $modelPayload->type;
                $model->is_public = $modelPayload->is_public;
                $model->is_active = $modelPayload->is_active;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
