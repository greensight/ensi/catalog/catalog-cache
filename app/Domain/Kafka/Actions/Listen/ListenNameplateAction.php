<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Nameplate\NameplateEventMessage;
use App\Domain\Offers\Models\Nameplate;
use RdKafka\Message;

class ListenNameplateAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = NameplateEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'name',
                'code',
                'background_color',
                'text_color',
                'is_active',
            ],
            findModel: fn () => Nameplate::query()->where('nameplate_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Nameplate();
                $model->nameplate_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel: function (Nameplate $model) use ($modelPayload) {
                $model->name = $modelPayload->name;
                $model->code = $modelPayload->code;
                $model->background_color = $modelPayload->background_color;
                $model->text_color = $modelPayload->text_color;
                $model->is_active = $modelPayload->is_active;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
