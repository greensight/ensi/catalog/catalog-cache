<?php

use App\Domain\Kafka\Actions\Listen\ListenPublishedPropertyValueAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedPropertyValue\PublishedPropertyValueEventMessage;
use App\Domain\Offers\Models\ProductPropertyValue;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-published-property-value');

test("Action ListenPublishedPropertyValueAction create success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $propertyValueId = 1;
    $productId = 1;
    $message = PublishedPropertyValueEventMessage::factory()
        ->attributes(['id' => $propertyValueId, 'deleted_at' => null, 'product_id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём оффер который связан с этим значением и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productId);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productId + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    assertDatabaseHas(ProductPropertyValue::class, [
        'product_property_value_id' => $propertyValueId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);

test("Action ListenPublishedPropertyValueAction update success", function (
    array $dirty,
    bool $isDirty,
    ?bool $always,
    bool $group,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ProductPropertyValue $propertyValue */
    $propertyValue = ProductPropertyValue::factory()->create();
    $updatedAt = $propertyValue->updated_at->addDay();
    $message = PublishedPropertyValueEventMessage::factory()
        ->attributes([
            'id' => $propertyValue->product_property_value_id,
            'product_id' => $propertyValue->product_id,
            'updated_at' => $updatedAt->toJSON(),
            'deleted_at' => null,
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер который связан с этим значением и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($propertyValue->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($propertyValue->product_id + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $propertyValue->updated_at;
    $propertyValue->refresh();
    assertEquals($needUpdatedAt, $propertyValue->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        if ($isDirty) {
            assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        } else {
            assertNewModelFieldEquals($groupOffer, 'updated_at');
        }
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with([
    [['name'], true],
    [['undefined'], false],
], FakerProvider::$optionalDataset, [true, false]);

test("Action ListenPublishedPropertyValueAction delete success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ProductPropertyValue $propertyValue */
    $propertyValue = ProductPropertyValue::factory()->create();
    $message = PublishedPropertyValueEventMessage::factory()
        ->attributes(['id' => $propertyValue->product_property_value_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, который связан с этим значением свойства и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($propertyValue->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($propertyValue->product_id + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    assertDatabaseMissing(ProductPropertyValue::class, ['product_property_value_id' => $propertyValue->id]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);

test("Action ListenPublishedPropertyValueAction delete_at success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ProductPropertyValue $propertyValue */
    $propertyValue = ProductPropertyValue::factory()->create();
    $message = PublishedPropertyValueEventMessage::factory()
        ->attributes([
            'id' => $propertyValue->product_property_value_id,
            'product_id' => $propertyValue->product_id,
            'deleted_at' => now()->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make();

    // Создаём оффер, который связан с этим значением свойства и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($propertyValue->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($propertyValue->product_id + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPublishedPropertyValueAction::class)->execute($message);

    assertDatabaseMissing(ProductPropertyValue::class, ['product_property_value_id' => $propertyValue->product_property_value_id]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);
