<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Kafka\Actions\Listen\ListenOfferAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Offer\OfferEventMessage;
use App\Domain\Offers\Models\Offer;
use Ensi\LaravelElasticQuery\ElasticClient;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertGreaterThan;
use function PHPUnit\Framework\assertNull;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-offer');

test("Action ListenOfferAction create success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $offerId = 1;
    $productId = 2;
    $message = OfferEventMessage::factory()->attributes(['id' => $offerId, 'product_id' => $productId])->event(ModelEventMessage::CREATE)->make();

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($productId);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenOfferAction::class)->execute($message);

    assertDatabaseHas(Offer::class, [
        'offer_id' => $offerId,
    ]);

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);

test("Action ListenOfferAction update success", function (
    array $dirty,
    bool $isDirty,
    bool $existDiscount,
    ?bool $always,
    bool $group,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Offer $offer */
    $offer = Offer::factory()->create(['updated_at' => now()->subDay(), 'price' => 100_00]);
    if ($existDiscount) {
        Discount::factory()->for($offer)->create();
    }
    $message = OfferEventMessage::factory()
        ->attributes([
            'id' => $offer->offer_id,
            'price' => $offer->base_price + 1,
            'external_id' => 'external_id',
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offer->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenOfferAction::class)->execute($message);

    $startUpdatedAt = $offer->updated_at;
    $offer->refresh();
    if ($isDirty) {
        assertGreaterThan($startUpdatedAt, $offer->updated_at);
        if (in_array('price', $dirty)) {
            $existDiscount ? assertNull($offer->price) : assertEquals($offer->price, $offer->base_price);
        }
    } else {
        assertEquals($offer->updated_at, $startUpdatedAt);
    }

    if ($group) {
        // Проверяем что офферы в одной группе правильно НЕ пометились
        assertNewModelFieldEquals($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with([
    [['price'], true, true],
    [['price'], true, false],
    [['external_id'], false, false],
], FakerProvider::$optionalDataset, [true, false]);

test("Action ListenOfferAction delete success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Offer $offer */
    $offer = Offer::factory()->create();
    $message = OfferEventMessage::factory()
        ->attributes(['id' => $offer->offer_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offer->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    $this->mock(ElasticClient::class)->shouldReceive('documentDelete')->times(1)->andReturn([]);

    resolve(ListenOfferAction::class)->execute($message);

    assertDatabaseMissing(Offer::class, ['offer_id' => $offer->offer_id]);

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);
