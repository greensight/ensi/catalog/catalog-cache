<?php

use App\Domain\Kafka\Actions\Listen\ListenProductGroupProductAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroupProduct\ProductGroupProductEventMessage;
use App\Domain\Offers\Models\ProductGroupProduct;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-product-group-product');

test("Action ListenProductGroupProductAction create success", function () {
    /** @var IntegrationTestCase $this */

    $productGroupProductId = 1;
    $productGroupId = 2;
    $productId = 3;
    $message = ProductGroupProductEventMessage::factory()
        ->attributes([
            'id' => $productGroupProductId,
            'product_id' => $productId,
            'product_group_id' => $productGroupId,
        ])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productId);
    // Создаём прочий и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productId + 1);

    // Подготавливаем оффер в одной группе с новым
    $groupOffer = OfferLinkedFactory::createGroupedProduct(productGroupId: $productGroupId);
    $otherOffer1 = OfferLinkedFactory::create();
    $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    assertDatabaseHas(ProductGroupProduct::class, [
        'product_group_product_id' => $productGroupProductId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    // Проверяем что офферы в одной группе правильно пометились
    assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
    assertNewModelFieldEquals($otherOffer1, 'updated_at');
    assertNewModelFieldEquals($otherOffer2, 'updated_at');
});

test("Action ListenProductGroupProductAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */

    /** @var ProductGroupProduct $productGroupProduct */
    $productGroupProduct = ProductGroupProduct::factory()->create();
    $updatedAt = $productGroupProduct->updated_at->addDay();
    $message = ProductGroupProductEventMessage::factory()
        ->attributes([
            'id' => $productGroupProduct->product_group_product_id,
            'product_id' => $productGroupProduct->product_id,
            'product_group_id' => $productGroupProduct->product_group_id,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productGroupProduct->product_id);
    // Создаём прочий и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productGroupProduct->product_id + 1);

    // Подготавливаем товар в одной группе с новым
    $groupOffer = OfferLinkedFactory::createGroupedProduct(productGroupId: $productGroupProduct->product_group_id);
    $otherOffer1 = OfferLinkedFactory::create();
    $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $productGroupProduct->updated_at;
    $productGroupProduct->refresh();
    assertEquals($needUpdatedAt, $productGroupProduct->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');

    // Проверяем что офферы в одной группе правильно пометились
    if ($isDirty) {
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
    } else {
        assertNewModelFieldEquals($groupOffer, 'updated_at');
    }
    assertNewModelFieldEquals($otherOffer1, 'updated_at');
    assertNewModelFieldEquals($otherOffer2, 'updated_at');
})->with([
    [['product_id'], true],
    [['is_active'], false],
]);

test("Action ListenProductGroupProductAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ProductGroupProduct $productGroupProduct */
    $productGroupProduct = ProductGroupProduct::factory()->create();
    $message = ProductGroupProductEventMessage::factory()
        ->attributes([
            'id' => $productGroupProduct->product_group_product_id,
        ])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productGroupProduct->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productGroupProduct->product_id + 1);

    // Подготавливаем офферы в одной группе с новым
    $groupOffer = OfferLinkedFactory::createGroupedProduct(productGroupId: $productGroupProduct->product_group_id);
    $otherOffer1 = OfferLinkedFactory::create();
    $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    assertDatabaseMissing(ProductGroupProduct::class, ['product_group_product_id' => $productGroupProduct->product_group_product_id]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    // Проверяем что офферы в одной группе правильно пометились
    assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
    assertNewModelFieldEquals($otherOffer1, 'updated_at');
    assertNewModelFieldEquals($otherOffer2, 'updated_at');
})->with(FakerProvider::$optionalDataset)->group('t');
