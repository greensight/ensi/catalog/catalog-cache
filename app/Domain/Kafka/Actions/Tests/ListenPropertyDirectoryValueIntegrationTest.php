<?php

use App\Domain\Kafka\Actions\Listen\ListenPropertyDirectoryValueAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PropertyDirectoryValue\PropertyDirectoryValueEventMessage;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-property-directory-value');

test("Action ListenPropertyDirectoryValueAction create success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $propertyDirectoryValueId = 1;
    $message = PropertyDirectoryValueEventMessage::factory()->attributes(['id' => $propertyDirectoryValueId])->event(ModelEventMessage::CREATE)->make();

    // Создаём оффер, который связан с этим PropertyDirectoryValue и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromDirectoryValue($propertyDirectoryValueId);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromDirectoryValue($propertyDirectoryValueId + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPropertyDirectoryValueAction::class)->execute($message);

    assertDatabaseHas(PropertyDirectoryValue::class, [
        'directory_value_id' => $propertyDirectoryValueId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);

test("Action ListenPropertyDirectoryValueAction update success", function (
    array $dirty,
    bool $isDirty,
    ?bool $always,
    bool $group,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var PropertyDirectoryValue $propertyDirectoryValue */
    $propertyDirectoryValue = PropertyDirectoryValue::factory()->create();
    $updatedAt = $propertyDirectoryValue->updated_at->addDay();
    $message = PropertyDirectoryValueEventMessage::factory()
        ->attributes(['id' => $propertyDirectoryValue->directory_value_id, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер, который связан с этим PropertyDirectoryValue и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromDirectoryValue($propertyDirectoryValue->directory_value_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromDirectoryValue($propertyDirectoryValue->directory_value_id + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPropertyDirectoryValueAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $propertyDirectoryValue->updated_at;
    $propertyDirectoryValue->refresh();
    assertEquals($needUpdatedAt, $propertyDirectoryValue->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        if ($isDirty) {
            assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        } else {
            assertNewModelFieldEquals($groupOffer, 'updated_at');
        }
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with([
    [['name'], true],
    [['undefined'], false],
], FakerProvider::$optionalDataset, [true, false]);

test("Action ListenPropertyDirectoryValueAction delete success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var PropertyDirectoryValue $propertyDirectoryValue */
    $propertyDirectoryValue = PropertyDirectoryValue::factory()->create();
    $message = PropertyDirectoryValueEventMessage::factory()
        ->attributes(['id' => $propertyDirectoryValue->directory_value_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, который связан с этим PropertyDirectoryValue и не должен пометиться на индексацию
    // т.к. при удалении не помечаем
    $offer = OfferLinkedFactory::createFromDirectoryValue($propertyDirectoryValue->directory_value_id);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offer->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPropertyDirectoryValueAction::class)->execute($message);

    assertDatabaseMissing(PropertyDirectoryValue::class, ['directory_value_id' => $propertyDirectoryValue->directory_value_id]);

    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($offer, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно НЕ пометились
        assertNewModelFieldEquals($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);
