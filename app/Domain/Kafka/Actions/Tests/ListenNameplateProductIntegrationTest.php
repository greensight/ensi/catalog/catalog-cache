<?php

namespace App\Domain\Kafka\Actions\Tests;

use App\Domain\Kafka\Actions\Listen\ListenNameplateProductAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\NameplateProduct\NameplateProductEventMessage;
use App\Domain\Offers\Models\NameplateProduct;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka');

test("Action ListenNameplateProductAction create success", function () {
    /** @var IntegrationTestCase $this */
    $nameplateProductId = 1;
    $productId = 1;
    $message = NameplateProductEventMessage::factory()
        ->attributes(['id' => $nameplateProductId, 'product_id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём оффер, которые содержит тег и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productId);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productId + 1);

    resolve(ListenNameplateProductAction::class)->execute($message);

    assertDatabaseHas(NameplateProduct::class, [
        'nameplate_product_id' => $nameplateProductId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
});

test("Action ListenNameplateProductAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var NameplateProduct $nameplateProduct */
    $nameplateProduct = NameplateProduct::factory()->create();
    $updatedAt = $nameplateProduct->updated_at->addDay();
    $message = NameplateProductEventMessage::factory()
        ->attributes([
            'id' => $nameplateProduct->nameplate_product_id,
            'product_id' => $nameplateProduct->product_id,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер, которые содержит тег и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($nameplateProduct->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($nameplateProduct->product_id + 1);

    resolve(ListenNameplateProductAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $nameplateProduct->updated_at;
    $nameplateProduct->refresh();
    assertEquals($needUpdatedAt, $nameplateProduct->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['product_id'], true],
    [['nameplate_id'], true],
]);

test("Action ListenNameplateProductAction delete success", function () {
    /** @var IntegrationTestCase $this */
    /** @var NameplateProduct $nameplateProduct */
    $nameplateProduct = NameplateProduct::factory()->create();
    $message = NameplateProductEventMessage::factory()
        ->attributes([
            'id' => $nameplateProduct->nameplate_product_id,
            'product_id' => $nameplateProduct->product_id,
        ])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, которые содержит тег и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($nameplateProduct->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($nameplateProduct->product_id + 1);

    resolve(ListenNameplateProductAction::class)->execute($message);

    assertDatabaseMissing(NameplateProduct::class, ['nameplate_product_id' => $nameplateProduct->nameplate_product_id]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
});
