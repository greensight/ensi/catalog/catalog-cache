<?php

namespace App\Domain\Kafka\Actions\Tests\Factories;

use App\Domain\Offers\Models\CategoryProductLink;
use App\Domain\Offers\Models\NameplateProduct;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Domain\Offers\Models\ProductPropertyValue;

class OfferLinkedFactory
{
    public static function create(?int $productId = null): Offer
    {
        $extra = [
            'created_at' => now()->subDay(),
            'updated_at' => now()->subDay(),
        ];
        if ($productId) {
            $extra['product_id'] = $productId;
        }

        return Offer::factory()->createQuietly($extra);
    }

    public static function createFromBrand(int $brandId): Offer
    {
        return static::create(static::createProduct(['brand_id' => $brandId])->product_id);
    }

    public static function createFromCategory(int $categoryId): Offer
    {
        $productId = static::createProduct()->product_id;
        CategoryProductLink::factory()->create(['category_id' => $categoryId, 'product_id' => $productId]);

        return static::create($productId);
    }

    public static function createFromDirectoryValue(int $directoryValueId): Offer
    {
        $product = static::createProduct();
        ProductPropertyValue::factory()->createQuietly([
            'directory_value_id' => $directoryValueId,
            'product_id' => $product->product_id,
        ]);

        return static::create($product->product_id);
    }

    public static function createFromNameplate(int $nameplateId): Offer
    {
        $product = static::createProduct();
        NameplateProduct::factory()->createQuietly([
            'nameplate_id' => $nameplateId,
            'product_id' => $product->product_id,
        ]);

        return static::create($product->product_id);
    }

    public static function createFromProperty(int $propertyId): Offer
    {
        $product = static::createProduct();
        ProductPropertyValue::factory()->createQuietly([
            'property_id' => $propertyId,
            'product_id' => $product->product_id,
        ]);

        return static::create($product->product_id);
    }

    public static function createFromCategoryAndProperty(int $categoryId, int $propertyId): Offer
    {
        $productId = static::createProduct()->product_id;
        CategoryProductLink::factory()->create(['category_id' => $categoryId, 'product_id' => $productId]);
        ProductPropertyValue::factory()->createQuietly([
            'property_id' => $propertyId,
            'product_id' => $productId,
        ]);

        return static::create($productId);
    }

    public static function createGroupedProduct(?int $productId = null, ?int $productGroupId = null): Offer
    {
        $product = static::createProduct();
        $offer = static::create($product->product_id);

        if (!$productGroupId) {
            /** @var ProductGroup $group */
            $group = ProductGroup::factory()->createQuietly();
            $productGroupId = $group->product_group_id;
        }

        $groupProductFactory = ProductGroupProduct::factory()->state(['product_group_id' => $productGroupId]);
        $groupProductFactory->createQuietly(['product_id' => $product->product_id]);
        if ($productId) {
            $groupProductFactory->createQuietly(['product_id' => $productId]);
        }

        return $offer;
    }

    protected static function createProduct(array $extra = []): Product
    {
        return Product::factory()->createQuietly($extra);
    }
}
