<?php

use App\Domain\Kafka\Actions\Listen\ListenActualCategoryPropertyAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ActualCategoryProperty\ActualCategoryPropertyEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Models\ActualCategoryProperty;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-actual-category-property');

test("Action ListenActualCategoryPropertyAction create success", function () {
    /** @var IntegrationTestCase $this */
    $actualCategoryPropertyId = 1;
    $propertyId = 1;
    $categoryId = 1;
    $message = ActualCategoryPropertyEventMessage::factory()
        ->attributes(['id' => $actualCategoryPropertyId, 'property_id' => $propertyId, 'category_id' => $categoryId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар и оффер, которые связаны с этой категорией и имеют свойство и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromCategoryAndProperty($categoryId, $propertyId);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromCategoryAndProperty($categoryId, $propertyId + 1);

    resolve(ListenActualCategoryPropertyAction::class)->execute($message);

    assertDatabaseHas(ActualCategoryProperty::class, [
        'actual_category_property_id' => $actualCategoryPropertyId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
});

test("Action ListenActualCategoryPropertyAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var ActualCategoryProperty $actualCategoryProperty */
    $actualCategoryProperty = ActualCategoryProperty::factory()->create();
    $updatedAt = $actualCategoryProperty->updated_at->addDay();
    $message = ActualCategoryPropertyEventMessage::factory()
        ->attributes([
            'id' => $actualCategoryProperty->actual_category_property_id,
            'property_id' => $actualCategoryProperty->property_id,
            'category_id' => $actualCategoryProperty->category_id,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём товар и оффер, которые связаны с этой категорией и имеют свойство и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromCategoryAndProperty($actualCategoryProperty->category_id, $actualCategoryProperty->property_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromCategoryAndProperty($actualCategoryProperty->category_id, $actualCategoryProperty->property_id + 1);

    resolve(ListenActualCategoryPropertyAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $actualCategoryProperty->updated_at;
    $actualCategoryProperty->refresh();
    assertEquals($needUpdatedAt, $actualCategoryProperty->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['is_gluing'], true],
    [['is_required'], false],
]);

test("Action ListenActualCategoryPropertyAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ActualCategoryProperty $actualCategoryProperty */
    $actualCategoryProperty = ActualCategoryProperty::factory()->create();
    $message = ActualCategoryPropertyEventMessage::factory()
        ->attributes([
            'id' => $actualCategoryProperty->actual_category_property_id,
        ])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар и оффер, которые связаны с этой категорией и имеют свойство и не должны пометиться на индексацию
    // т.к. при удалении не помечаем
    $offer = OfferLinkedFactory::createFromCategoryAndProperty($actualCategoryProperty->category_id, $actualCategoryProperty->property_id);

    resolve(ListenActualCategoryPropertyAction::class)->execute($message);

    assertDatabaseMissing(ActualCategoryProperty::class, ['actual_category_property_id' => $actualCategoryProperty->actual_category_property_id]);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($offer, 'updated_at');
})->with(FakerProvider::$optionalDataset);
