<?php

use App\Domain\Kafka\Actions\Listen\ListenPublishedImageAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedImage\PublishedImageEventMessage;
use App\Domain\Offers\Models\Image;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-published-image');

test("Action ListenPublishedImageAction create success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $imageId = 1;
    $productId = 1;
    $message = PublishedImageEventMessage::factory()
        ->attributes(['id' => $imageId, 'product_id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём оффер который связан с этим изображением и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productId);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productId + 1);

    resolve(ListenPublishedImageAction::class)->execute($message);

    assertDatabaseHas(Image::class, [
        'image_id' => $imageId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenPublishedImageAction update success", function (array $dirty, bool $isDirty, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Image $image */
    $image = Image::factory()->create();
    $updatedAt = $image->updated_at->addDay();
    $message = PublishedImageEventMessage::factory()
        ->attributes(['id' => $image->image_id, 'updated_at' => $updatedAt->toJSON(), 'product_id' => $image->product_id])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер который связан с этим изображением и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($image->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($image->product_id + 1);

    resolve(ListenPublishedImageAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $image->updated_at;
    $image->refresh();
    assertEquals($needUpdatedAt, $image->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['name'], true],
    [['is_active'], false],
], FakerProvider::$optionalDataset);

test("Action ListenPublishedImageAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Image $image */
    $image = Image::factory()->create();
    $message = PublishedImageEventMessage::factory()
        ->attributes(['id' => $image->image_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, который связан с этим изображением и должен пометиться на индексацию
    $offer = OfferLinkedFactory::create($image->product_id);

    resolve(ListenPublishedImageAction::class)->execute($message);

    assertDatabaseMissing(Image::class, ['image_id' => $image->image_id]);
    assertNewModelFieldGreaterThan($offer, 'updated_at');
})->with(FakerProvider::$optionalDataset);
