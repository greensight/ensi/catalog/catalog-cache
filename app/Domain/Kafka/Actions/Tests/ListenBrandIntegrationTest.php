<?php

use App\Domain\Kafka\Actions\Listen\ListenBrandAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Brand\BrandEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Models\Brand;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-brand');

test("Action ListenBrandAction create success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $brandId = 1;
    $message = BrandEventMessage::factory()->attributes(['id' => $brandId])->event(ModelEventMessage::CREATE)->make();

    // Создаём товар и оффер, которые связаны с этим брендом и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromBrand($brandId);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromBrand($brandId + 1);

    resolve(ListenBrandAction::class)->execute($message);

    assertDatabaseHas(Brand::class, [
        'brand_id' => $brandId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenBrandAction update success", function (array $dirty, bool $isDirty, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Brand $brand */
    $brand = Brand::factory()->create();
    $updatedAt = $brand->updated_at->addDay();
    $message = BrandEventMessage::factory()
        ->attributes(['id' => $brand->brand_id, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём товар и оффер, которые связаны с этим брендом и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromBrand($brand->brand_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromBrand($brand->brand_id + 1);

    resolve(ListenBrandAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $brand->updated_at;
    $brand->refresh();
    assertEquals($needUpdatedAt, $brand->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['name'], true],
    [['is_active'], false],
], FakerProvider::$optionalDataset);

test("Action ListenBrandAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Brand $brand */
    $brand = Brand::factory()->create();
    $message = BrandEventMessage::factory()
        ->attributes(['id' => $brand->brand_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар и оффер, которые связаны с этим брендом и не должны пометиться на индексацию
    // т.к. при удалении не помечаем
    $offer = OfferLinkedFactory::createFromBrand($brand->brand_id);

    resolve(ListenBrandAction::class)->execute($message);

    assertDatabaseMissing(Brand::class, ['brand_id' => $brand->brand_id]);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($offer, 'updated_at');
})->with(FakerProvider::$optionalDataset);
