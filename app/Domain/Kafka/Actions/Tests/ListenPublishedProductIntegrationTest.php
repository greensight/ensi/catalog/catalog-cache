<?php

use App\Domain\Kafka\Actions\Listen\ListenPublishedProductAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct\PublishedProductEventMessage;
use App\Domain\Offers\Models\Product;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-published-product');

test("Action ListenPublishedProductAction create success", function (bool $allowPublish, ?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $productId = 1;
    $message = PublishedProductEventMessage::factory()->attributes(['id' => $productId, 'allow_publish' => $allowPublish])->event(ModelEventMessage::CREATE)->make();

    // Создаём оффер который связан с этим товаром и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productId);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productId + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($productId);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPublishedProductAction::class)->execute($message);

    if ($allowPublish) {
        assertDatabaseHas(Product::class, ['product_id' => $productId]);
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
        if ($group) {
            // Проверяем что офферы в одной группе правильно пометились
            assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
            assertNewModelFieldEquals($otherOffer1, 'updated_at');
            assertNewModelFieldEquals($otherOffer2, 'updated_at');
        }
    } else {
        assertDatabaseMissing(Product::class, ['product_id' => $productId]);
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
})->with([true, false], FakerProvider::$optionalDataset, [true, false]);

test("Action ListenPublishedProductAction update success", function (
    array $dirty,
    bool $isDirty,
    bool $allowPublish,
    ?bool $always,
    bool $group,
) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Product $product */
    $product = Product::factory()->create();
    $updatedAt = $product->updated_at->addDay();
    $message = PublishedProductEventMessage::factory()
        ->attributes(['id' => $product->product_id, 'allow_publish' => $allowPublish, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер который связан с этим товаром и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($product->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($product->product_id + 1);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPublishedProductAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $product->updated_at;
    $product->refresh();
    assertEquals($needUpdatedAt, $product->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        if ($isDirty) {
            assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        } else {
            assertNewModelFieldEquals($groupOffer, 'updated_at');
        }
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with([
    [['name'], true, true],
    [['is_active'], false, true],
    [['allow_publish'], true, false],
    [[], false, false],
], FakerProvider::$optionalDataset, [true, false]);

test("Action ListenPublishedProductAction delete success", function (?bool $always, bool $group) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Product $product */
    $product = Product::factory()->create();
    $message = PublishedProductEventMessage::factory()
        ->attributes(['id' => $product->product_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, который связан с этим товаром и должен пометиться на индексацию
    $offer = OfferLinkedFactory::create($product->product_id);

    if ($group) {
        // Подготавливаем офферы в одной группе
        $groupOffer = OfferLinkedFactory::createGroupedProduct($product->product_id);
        $otherOffer1 = OfferLinkedFactory::create();
        $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);
    }

    resolve(ListenPublishedProductAction::class)->execute($message);

    assertDatabaseMissing(Product::class, ['product_id' => $product->product_id]);
    assertNewModelFieldGreaterThan($offer, 'updated_at');

    if ($group) {
        // Проверяем что офферы в одной группе правильно пометились
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
        assertNewModelFieldEquals($otherOffer1, 'updated_at');
        assertNewModelFieldEquals($otherOffer2, 'updated_at');
    }
})->with(FakerProvider::$optionalDataset)->with([true, false]);
