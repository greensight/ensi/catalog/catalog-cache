<?php

use App\Domain\Kafka\Actions\Listen\ListenProductGroupAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroup\ProductGroupEventMessage;
use App\Domain\Offers\Models\ProductGroup;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-product-group');

test("Action ListenProductGroupAction create success", function () {
    /** @var IntegrationTestCase $this */
    $productGroupId = 1;
    $message = ProductGroupEventMessage::factory()
        ->attributes(['id' => $productGroupId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create();
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($offerMark->product_id + 1);

    // Подготавливаем офферы в одной группе + связываем $offerMark с создаваемой группой
    $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id, $productGroupId);
    $otherOffer1 = OfferLinkedFactory::create();
    $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);

    resolve(ListenProductGroupAction::class)->execute($message);

    assertDatabaseHas(ProductGroup::class, [
        'product_group_id' => $productGroupId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    // Проверяем что офферы в одной группе правильно пометились
    assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
    assertNewModelFieldEquals($otherOffer1, 'updated_at');
    assertNewModelFieldEquals($otherOffer2, 'updated_at');
});

test("Action ListenProductGroupAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create();
    $updatedAt = $productGroup->updated_at->addDay();
    $message = ProductGroupEventMessage::factory()
        ->attributes([
            'id' => $productGroup->product_group_id,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create();
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($offerMark->product_id + 1);

    // Подготавливаем офферы в одной группе + связываем $offerMark с создаваемой группой
    $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id, $productGroup->product_group_id);
    $otherOffer1 = OfferLinkedFactory::create();
    $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);

    resolve(ListenProductGroupAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $productGroup->updated_at;
    $productGroup->refresh();
    assertEquals($needUpdatedAt, $productGroup->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');

    // Проверяем что офферы в одной группе правильно пометились
    if ($isDirty) {
        assertNewModelFieldGreaterThan($groupOffer, 'updated_at');
    } else {
        assertNewModelFieldEquals($groupOffer, 'updated_at');
    }
    assertNewModelFieldEquals($otherOffer1, 'updated_at');
    assertNewModelFieldEquals($otherOffer2, 'updated_at');
})->with([
    [['name'], true],
    [['is_active'], true],
    [['product_id'], false],
]);

test("Action ListenProductGroupAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create();
    $message = ProductGroupEventMessage::factory()
        ->attributes([
            'id' => $productGroup->product_group_id,
        ])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create();
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($offerMark->product_id + 1);

    // Подготавливаем офферы в одной группе + связываем $offerMark с создаваемой группой
    $groupOffer = OfferLinkedFactory::createGroupedProduct($offerMark->product_id, $productGroup->product_group_id);
    $otherOffer1 = OfferLinkedFactory::create();
    $otherOffer2 = OfferLinkedFactory::createGroupedProduct($otherOffer1->product_id);

    resolve(ListenProductGroupAction::class)->execute($message);

    assertDatabaseMissing(ProductGroup::class, ['product_group_id' => $productGroup->product_group_id]);
    // Не должно помечаться, т.к. ждём событие удаления ProductGroupProduct
    assertNewModelFieldEquals($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');

    // Проверяем что офферы в одной группе правильно НЕ пометились
    assertNewModelFieldEquals($groupOffer, 'updated_at');
    assertNewModelFieldEquals($otherOffer1, 'updated_at');
    assertNewModelFieldEquals($otherOffer2, 'updated_at');
})->with(FakerProvider::$optionalDataset);
