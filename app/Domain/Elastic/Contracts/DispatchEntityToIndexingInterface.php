<?php

namespace App\Domain\Elastic\Contracts;

interface DispatchEntityToIndexingInterface
{
    public function execute(): void;
}
