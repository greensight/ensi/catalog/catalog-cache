<?php

namespace App\Domain\Elastic\Tests\Factories;

use App\Domain\Elastic\Concerns\ElasticIndex;
use Ensi\LaravelTestFactories\BaseApiFactory;

class IndexFactory extends BaseApiFactory
{
    protected ElasticIndex $index;

    protected function definition(): array
    {
        $staticIndexName = $this->index
            ->nameData()
            ->toStringWithoutHash();

        return [
            'index' => $staticIndexName . '_' . $this->faker->ean8(),
            'creation.date' => $this->faker->dateTime(),
        ];
    }

    public function setIndex(ElasticIndex $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
