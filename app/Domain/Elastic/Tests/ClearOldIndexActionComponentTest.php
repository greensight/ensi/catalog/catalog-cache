<?php

namespace App\Domain\Elastic\Tests;

use App\Domain\Elastic\Actions\ClearOldIndexAction;
use App\Domain\Elastic\Tests\Cases\ApiV1ClearOldIndexComponentTestCase;
use Carbon\Carbon;

uses(ApiV1ClearOldIndexComponentTestCase::class);
uses()->group('component', 'ClearOldIndexAction');

test("Action ClearOldIndexAction stages success", function () {
    /** @var ApiV1ClearOldIndexComponentTestCase $this */
    $datetime = Carbon::now()->addDay()->startOfDay();
    // Отличаются только стейджами
    $this->stage('missing')->createMissingValues();
    $this->stage('exists')->createExistsValues();

    resolve(ClearOldIndexAction::class)->execute(datetime: $datetime, stages: ['missing']);

    $this->assert();
});
