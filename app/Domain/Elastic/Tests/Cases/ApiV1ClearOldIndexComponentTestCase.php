<?php

namespace App\Domain\Elastic\Tests\Cases;

use App\Domain\Elastic\Actions\ApplyIndexAction;
use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\LaravelElasticQuery\ElasticClient;

use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertTrue;

abstract class ApiV1ClearOldIndexComponentTestCase extends ApiV1ComponentTestCase
{
    protected array $missingIndexes = [];
    protected array $missingTimestamps = [];
    protected array $existsIndexes = [];
    protected array $existsTimestamps = [];

    public function createMissingValues(array $params = []): self
    {
        resolve(ApplyIndexAction::class)->execute(
            function (ElasticIndex $index) use ($params) {
                $index->createIfNotExist();
                $this->missingIndexes[$index->indexName()] = $index;
                $this->missingTimestamps[] = IndexerTimestamp::factory()->forIndex($index)->create($params);
            }
        );

        return $this;
    }

    public function createExistsValues(array $params = []): self
    {
        resolve(ApplyIndexAction::class)->execute(
            function (ElasticIndex $index) use ($params) {
                $index->createIfNotExist();
                $this->existsIndexes[$index->indexName()] = $index;
                $this->existsTimestamps[] = IndexerTimestamp::factory()->forIndex($index)->create($params);
            }
        );

        return $this;
    }

    public function assert(): void
    {
        /** @var ElasticClient $client */
        $client = resolve(ElasticClient::class);

        // missing
        foreach ($this->missingIndexes as $indexName => $index) {
            assertFalse($client->indicesExists($indexName));
        }

        foreach ($this->missingTimestamps as $timestamp) {
            assertModelMissing($timestamp);
        }

        // exists
        foreach ($this->existsIndexes as $index) {
            assertTrue($index->isCreated());
        }

        foreach ($this->existsTimestamps as $timestamp) {
            assertModelExists($timestamp);
        }
    }
}
