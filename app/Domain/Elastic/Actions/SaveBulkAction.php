<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Exceptions\ExceptionFormatter;
use Psr\Log\LoggerInterface;
use Throwable;

class SaveBulkAction
{
    public function execute(ElasticIndex $index, LoggerInterface $logger, array $bulk, array $logContext): void
    {
        if (!$bulk) {
            return;
        }

        try {
            $logger->info("Индекс: {$index->indexName()}. Отправка пакета в индекс", $logContext);
            $responses = $index->bulk($bulk);

            foreach ($responses['items'] as $response) {
                $indexResponse = $response['index'] ?? null;
                if ($indexResponse) {
                    $error = $indexResponse['error'] ?? null;
                    if ($error) {
                        $logger->error("Индекс: {$index->indexName()}. Ошибка индексации, _id: {$indexResponse['_id']}", [
                            'error' => $error,
                        ]);
                    } else {
                        $logger->info("Индекс: {$index->indexName()}. Успешная индексация, _id: {$indexResponse['_id']}");
                    }
                }
            }
        } catch (Throwable $e) {
            $logger->error("Индекс: {$index->indexName()}. " . ExceptionFormatter::line("Ошибка при индексации", $e), $logContext);
        }
    }
}
