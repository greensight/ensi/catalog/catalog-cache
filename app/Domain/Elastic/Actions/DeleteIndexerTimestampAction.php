<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Models\IndexerTimestamp;

class DeleteIndexerTimestampAction
{
    public function execute(int $modelId): void
    {
        IndexerTimestamp::destroy($modelId);
    }
}
