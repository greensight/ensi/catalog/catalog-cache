<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Contracts\DispatchEntityToIndexingInterface;

class ElasticRecordProcessAction
{
    public function execute(): void
    {
        foreach (config('elastic.indexes') as $indexClass => $settings) {
            /** @var DispatchEntityToIndexingInterface $action */
            $action = resolve($settings['scan']);
            $action->execute();
        }
    }
}
