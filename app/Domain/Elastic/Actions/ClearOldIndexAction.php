<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Data\ElasticIndexNameData;
use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Exceptions\ExceptionFormatter;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Ensi\LaravelElasticQuery\ElasticClient;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;
use Throwable;

class ClearOldIndexAction
{
    public const DAYS = 30;

    protected LoggerInterface $logger;
    /** @var Collection<string,IndexerTimestamp> */
    protected Collection $timestamps;

    public function __construct(protected ElasticClient $client)
    {
        $this->logger = logger()->channel('elastic:clear');
    }

    public function execute(
        CarbonInterface $datetime,
        ?array $stages = null,
        ?array $hashes = null
    ): void {
        $indices = $this->client->indicesInfo(
            indices: [ElasticIndexNameData::appIndicesPattern()],
            columns: ['i', 'creation.date.string'],
        );

        $this->fetchTimestamps($datetime, $stages, $hashes);

        foreach ($indices as $indexInfo) {
            $indexName = $indexInfo['i'];

            $date = Carbon::parse($indexInfo['creation.date.string']);
            if ($date->gt($datetime)) {
                continue;
            }

            $indexNameData = ElasticIndexNameData::parseFromIndexName($indexName);

            // If you have passed a list of stages, then we select indexes only among them
            if (is_array($stages) && !in_array($indexNameData->stage, $stages)) {
                continue;
            }

            // If we have passed a list of hashes, then we select indexes only among them
            if (is_array($hashes) && !in_array($indexNameData->hash, $hashes)) {
                continue;
            }

            // We delete it if the index is outdated and it is not in the IndexerTimestamp
            if (!$this->timestamps->has($indexName)) {
                $this->delete($indexName);

                continue;
            }

            // We delete it if the index is outdated and the last_schedule entry in IndexerTimestamp is also outdated
            /** @var IndexerTimestamp $timestamp */
            $timestamp = $this->timestamps->get($indexName);
            if ($timestamp->last_schedule->lte($datetime)) {
                $this->delete($indexName);
            }
        }
    }

    protected function fetchTimestamps(CarbonInterface $datetime, ?array $stages = null, ?array $hashes = null): void
    {
        $timestampQuery = IndexerTimestamp::query();

        if ($stages) {
            $timestampQuery->whereIn('stage', $stages);
        }

        if ($hashes) {
            $timestampQuery->whereIn('index_hash', $hashes);
        }

        $timestampQuery->clone()
            ->whereDate('last_schedule', '<', $datetime)
            ->delete();

        /** @var IndexerTimestamp[] $timestamps */
        $this->timestamps = $timestampQuery->get()->keyBy(function (IndexerTimestamp $t) {
            $indexName = new ElasticIndexNameData(
                index: $t->index,
                hash: $t->index_hash,
                stage: $t->stage,
            );

            return $indexName->toString();
        });
    }

    protected function delete(string $indexName): void
    {
        $this->logger->info("Index: {$indexName}. Starting delete");

        try {
            $this->logger->info("Deleting index: {$indexName}");
            $this->client->indicesDelete($indexName);
            $this->logger->info("Index: {$indexName}. Index deleted");

            if ($this->timestamps->has($indexName)) {
                $this->logger->info("Index: {$indexName}. Deleting IndexerTimestamp");
                $this->timestamps->get($indexName)->delete();
                $this->logger->info("Index: {$indexName}. IndexerTimestamp deleted");
            }
        } catch (Throwable $e) {
            $this->logger->error("Index: {$indexName}. " . ExceptionFormatter::line("Error while deleting", $e));
        }
    }
}
