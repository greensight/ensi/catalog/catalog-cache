<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Models\IndexerTimestamp;

class PatchIndexerTimestampAction
{
    public function execute(int $modelId, array $fields): IndexerTimestamp
    {
        /** @var IndexerTimestamp $model */
        $model = IndexerTimestamp::query()->findOrFail($modelId);
        $model->update($fields);

        return $model;
    }
}
