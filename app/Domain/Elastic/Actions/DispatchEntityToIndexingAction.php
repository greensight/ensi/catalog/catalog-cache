<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Exceptions\ExceptionFormatter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;
use Throwable;

class DispatchEntityToIndexingAction
{
    public function execute(
        ElasticIndex $index,
        callable $queryBuilder,
        callable $dispatch,
        LoggerInterface $logger
    ): void {
        $logger->info("=== Начато сканирование новых изменений для " . $index::class);

        try {
            /** @var IndexerTimestamp|null $lastIndexer */
            $lastIndexer = IndexerTimestamp::query()->whereIndex($index)->first();
            $now = now();

            if ($lastIndexer) {
                $logger->info("Ищу записи от " . $lastIndexer->last_schedule->toISOString());
            } else {
                $lastIndexer = IndexerTimestamp::makeForIndex($index);
            }
            $logger->info("Ищу записи до " . $now->toISOString());

            /** @var Builder $query */
            $query = $queryBuilder($lastIndexer->last_schedule, $now);

            $query->chunk(500, function (Collection $results) use ($logger, $dispatch) {
                $ids = $results->pluck('dispatch_id');

                try {
                    $logger->info("Запускаю для индексации: {$ids->count()} шт.", [
                        'ids' => $ids->all(),
                    ]);
                    $dispatch($ids->all());
                    $logger->info("Запущено");
                } catch (Throwable $e) {
                    $logger->error(ExceptionFormatter::line("Ошибка при запуске", $e));
                }
            });

            $lastIndexer->last_schedule = $now;
            $lastIndexer->save();
        } catch (Throwable $e) {
            $logger->error(ExceptionFormatter::line("Ошибка при сканировании записей на индексацию", $e));
        }

        $logger->info("=== Закончено сканирование новых изменений");
    }
}
