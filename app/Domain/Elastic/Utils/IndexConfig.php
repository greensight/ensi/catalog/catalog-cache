<?php

namespace App\Domain\Elastic\Utils;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Data\ElasticIndexNameData;

/**
 * Full name of index: (stage + app + index_name + hash)
 * Example: (dev_catalog_cache + catalog_cache + contents + 815fd622)
 */
class IndexConfig
{
    public const SALT = "v1";

    public static function static(callable $fn): void
    {
        foreach (config('elastic.indexes') as $indexClass => $settings) {
            $fn($indexClass, $settings);
        }
    }

    public static function object(callable $fn): void
    {
        foreach (config('elastic.indexes') as $indexClass => $settings) {
            /** @var ElasticIndex $indexClass */

            $fn($indexClass::build());
        }
    }

    public static function index(string|ElasticIndex $index): ElasticIndex
    {
        return is_string($index) ? new $index() : $index;
    }

    public static function hashFromArray(array $settings): string
    {
        return substr(md5(json_encode($settings) . static::SALT), 0, 8);
    }

    public static function nameData(string|ElasticIndex|ElasticIndexNameData $index): ElasticIndexNameData
    {
        if ($index instanceof ElasticIndexNameData) {
            return $index;
        }

        return static::index($index)->nameData();
    }
}
