<?php

namespace App\Domain\Elastic\Utils;

use Ensi\LaravelMetrics\Guzzle\GuzzleMiddleware as MetricsMiddleware;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;

class HttpClientOptionsBuilder
{
    public static function getHttpClientOptions(): array
    {
        $handler = new CurlHandler();
        $stack = new HandlerStack($handler);

        $stack->push(MetricsMiddleware::middleware('elastic_client'));

        if (config('logging.http_logger_enable')) {
            $stack->push(self::configureLoggerMiddleware());
        }

        return ['handler' => $stack];
    }

    private static function configureLoggerMiddleware(): callable
    {
        $logger = logger()->channel('elastic_client');
        $format = "{req_headers}\n{req_body}\n\n{res_headers}\n{res_body}\n\n";
        $formatter = new MessageFormatter($format);

        return Middleware::log($logger, $formatter, 'debug');
    }
}
