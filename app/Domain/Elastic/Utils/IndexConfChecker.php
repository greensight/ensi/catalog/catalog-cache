<?php

namespace App\Domain\Elastic\Utils;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Data\ElasticIndexNameData;

class IndexConfChecker
{
    public static function checkIndex(string $indexName, string $indexHash): bool
    {
        foreach (config('elastic.indexes') as $indexClass => $settings) {
            /** @var ElasticIndex $index */
            $index = new $indexClass();

            if ($indexName == $index->indexBaseName() && $indexHash == $index->settingsHash()) {
                return true;
            }
        }

        return false;
    }

    public static function checkStage(string $stage): bool
    {
        return $stage == ElasticIndexNameData::stageName();
    }
}
