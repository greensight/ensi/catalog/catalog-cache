<?php

namespace App\Domain\Elastic\Concerns;

use App\Domain\Elastic\Data\ElasticIndexNameData;
use App\Domain\Elastic\Tests\Factories\IndexFactory;
use App\Domain\Elastic\Utils\IndexConfig;
use App\Exceptions\ExceptionFormatter;
use Ensi\LaravelElasticQuery\ElasticIndex as BaseElasticIndex;
use Throwable;

abstract class ElasticIndex extends BaseElasticIndex
{
    public static function build(): static
    {
        return new static();
    }

    public function indexName(): string
    {
        return $this->nameData()->toString();
    }

    public function nameData(): ElasticIndexNameData
    {
        return new ElasticIndexNameData(
            index: $this->name,
            hash: $this->settingsHash(),
        );
    }

    public function indexBaseName(): string
    {
        return $this->name;
    }

    public function settingsHash(): string
    {
        $settings = $this->settings();
        unset($settings['settings']['analysis']['filter']['synonym']);

        return IndexConfig::hashFromArray($settings);
    }

    public function createIfNotExist(): ?bool
    {
        $logger = logger()->channel('elastic:create');

        try {
            if ($this->isCreated()) {
                return null;
            }

            $logger->info("Index: {$this->indexName()}. Begin creating");
            $this->create();
            $logger->info("Index: {$this->indexName()}. Created");

            return true;
        } catch (Throwable $e) {
            $logger->error("Index: {$this->indexName()}. " . ExceptionFormatter::line("Error while creating", $e));

            return false;
        }
    }

    public static function factory(): IndexFactory
    {
        return IndexFactory::new()->setIndex(new static());
    }
}
