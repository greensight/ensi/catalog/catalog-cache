<?php

namespace App\Domain\Elastic\Data;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Utils\IndexConfig;

class ElasticIndexNameData extends ElasticNameData
{
    public function __construct(
        public string $index,
        public string $hash,
        ?string $stage = null,
        ?string $app = null,
    ) {
        $this->index = static::prepare($this->index);
        $this->hash = static::prepare($this->hash);

        parent::__construct($stage, $app);
    }

    public function toString(): string
    {
        return static::makeName([$this->stage, $this->app, $this->index, $this->hash]);
    }

    public function toStringWithoutHash(): string
    {
        return static::makeName([$this->stage, $this->app, $this->index]);
    }

    public static function appIndicesPattern(): string
    {
        return '*_' . static::appName() . '_*';
    }

    public static function parseFromIndexName(string $indexName): ?static
    {
        $stage = '(.*)';
        $app = static::appName();
        $indexTypes = [];
        IndexConfig::object(function (ElasticIndex $index) use (&$indexTypes) {
            $indexTypes[] = $index->nameData()->index;
        });
        $indexType = '(' . join('|', $indexTypes) . ')';
        $hash = '([a-z0-9]{8})';

        $matches = [];
        preg_match("/^{$stage}_{$app}_{$indexType}_{$hash}$/", $indexName, $matches);
        if (!$matches) {
            return null;
        }

        return new static(
            index: $matches[2],
            hash: $matches[3],
            stage: $matches[1],
        );
    }
}
