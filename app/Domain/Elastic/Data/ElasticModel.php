<?php

namespace App\Domain\Elastic\Data;

use Illuminate\Support\Fluent;

abstract class ElasticModel extends Fluent
{
    abstract public function getId(): string|int;
}
