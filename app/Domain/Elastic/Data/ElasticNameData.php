<?php

namespace App\Domain\Elastic\Data;

class ElasticNameData
{
    public string $app;
    public string $stage;

    public function __construct(
        ?string $stage = null,
        ?string $app = null,
    ) {
        $this->app = static::prepare($app ?: static::appName());
        $this->stage = static::prepare($stage ?: static::stageName());
    }

    public static function appName(): string
    {
        return static::prepare(config('app.name'));
    }

    public static function stageName(): string
    {
        return static::prepare(config('app.stage'));
    }

    public static function toStringStageApp(): string
    {
        return static::makeName([static::stageName(), static::appName()]);
    }

    protected static function makeName(array $names): string
    {
        return join('_', $names);
    }

    protected static function prepare(string $string): string
    {
        return strtolower(str_replace(['.', '-', ' '], '', $string));
    }
}
