<?php

namespace App\Domain\Elastic\Models;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Data\ElasticIndexNameData;
use App\Domain\Elastic\Models\Tests\Factories\IndexerTimestampFactory;
use App\Domain\Elastic\Utils\IndexConfig;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property string $index The base name of the index (products, categories)
 * @property string $stage The branch where indexing takes place
 * @property string $index_hash Index hash
 *
 * @property CarbonInterface $last_schedule
 */
class IndexerTimestamp extends Model
{
    protected $table = 'indexer_timestamps';

    public $timestamps = false;

    protected $fillable = ['last_schedule'];

    protected $casts = [
        'last_schedule' => 'datetime',
    ];

    public static function factory(): IndexerTimestampFactory
    {
        return IndexerTimestampFactory::new();
    }

    public function scopeWhereIndex(Builder $builder, ElasticIndex $index, ?string $hash = null): Builder
    {
        return $builder->where('index', $index->indexBaseName())
            ->where('stage', config('app.stage'))
            ->where('index_hash', $hash ?: $index->settingsHash());
    }

    public function scopeWhereIsCurrent(Builder $builder): Builder
    {
        return $builder->where(function (Builder $blockIndexQuery) {
            foreach (config('elastic.indexes') as $indexName => $settings) {
                $indexClass = app($indexName);
                $blockIndexQuery->orWhere(fn (Builder $query) => $query->whereIndex($indexClass));
            }
        });
    }

    public static function makeForIndex(string|ElasticIndex|ElasticIndexNameData $indexClass): static
    {
        $nameData = IndexConfig::nameData($indexClass);

        $model = new static();
        $model->index = $nameData->index;
        $model->stage = $nameData->stage;
        $model->index_hash = $nameData->hash;

        return $model;
    }
}
