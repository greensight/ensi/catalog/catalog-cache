<?php

namespace App\Domain\Elastic\Models\Tests\Factories;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Data\ElasticIndexNameData;
use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Domain\Elastic\Utils\IndexConfig;
use Ensi\LaravelTestFactories\BaseModelFactory;

class IndexerTimestampFactory extends BaseModelFactory
{
    protected $model = IndexerTimestamp::class;

    public function definition(): array
    {
        return [
            'index' => $this->faker->word(),
            'stage' => ElasticIndexNameData::stageName(),
            'index_hash' => $this->faker->ean8(),
            'last_schedule' => $this->faker->dateTime(),
        ];
    }

    public function forIndex(string|ElasticIndex|ElasticIndexNameData $indexClass): static
    {
        $nameData = IndexConfig::nameData($indexClass);

        return $this->state([
            'index' => $nameData->index,
            'index_hash' => $nameData->hash,
        ]);
    }
}
