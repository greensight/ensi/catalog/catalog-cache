<?php

namespace App\Domain\Discounts\Models;

use App\Domain\Discounts\Models\Tests\Factories\DiscountFactory;
use App\Domain\Offers\Models\Offer;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $offer_id Offer ID from Offers
 *
 * @property int $value_type Value type
 * @property int $value Discount value
 *
 * @property Offer $offer Attached offer
 *
 * @property CarbonInterface|null $created_at Date of creation (in the current service, not an external one)
 * @property CarbonInterface|null $updated_at Update date (in the current service, not an external one)
 */
class Discount extends Model
{
    protected $table = 'discounts';

    protected $fillable = ['value', 'value_type'];

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class, 'offer_id', 'offer_id');
    }

    public static function factory(): DiscountFactory
    {
        return DiscountFactory::new();
    }
}
