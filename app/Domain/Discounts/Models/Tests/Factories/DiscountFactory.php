<?php

namespace App\Domain\Discounts\Models\Tests\Factories;

use App\Domain\Discounts\Models\Discount;
use Ensi\LaravelTestFactories\BaseModelFactory;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;

class DiscountFactory extends BaseModelFactory
{
    protected $model = Discount::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'value_type' => $this->faker->randomElement(DiscountValueTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }
}
