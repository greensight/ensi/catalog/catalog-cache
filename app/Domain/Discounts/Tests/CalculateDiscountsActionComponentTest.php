<?php

namespace App\Domain\Discounts\Tests;

use App\Domain\Discounts\Actions\CalculateDiscountsAction;
use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Tests\Factories\CalculateCatalogResponseFactory;
use App\Domain\Discounts\Tests\Factories\CalculatedOfferFactory;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\MarketingClient\Dto\CalculateCatalogRequest;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Illuminate\Database\Eloquent\Collection as DatabaseCollection;
use Illuminate\Support\Arr;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEqualsCanonicalizing;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'discounts');

test("Action CalculateDiscountsAction check marketing price", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ComponentTestCase $this */
    $discountValue = 5_00;
    /** @var Category $category */
    $category = Category::factory()->create();

    /** @var Offer $offerWithPrice */
    $offerWithPrice = Offer::factory()->withBasePrice()->create(['price' => 10_00]);
    /** @var Discount $discountOfferWithPrice */
    $discountOfferWithPrice = Discount::factory()->for($offerWithPrice)->create();
    /** @var Offer $offerWithCalc */
    $offerWithCalc = Offer::factory()->withBasePrice()->create(['price' => null]);
    $offerWithCalcPrice = $offerWithCalc->base_price - 5_00;
    /** @var Offer $offerWithBadCalc */
    $offerWithBadCalc = Offer::factory()->withBasePrice()->create(['price' => null]);

    Product::factory()
        ->count(3)
        ->sequence(
            ['product_id' => $offerWithBadCalc->product_id],
            ['product_id' => $offerWithPrice->product_id],
            ['product_id' => $offerWithCalc->product_id],
        )
        ->inCategories([$category])
        ->create(['brand_id' => null]);

    $offer = CalculatedOfferFactory::new()
        ->withDiscounts([
            'value' => $discountValue,
            'value_type' => DiscountValueTypeEnum::RUB,
        ])
        ->make([
            'offer_id' => $offerWithCalc->offer_id,
            'price' => $offerWithCalcPrice,
        ]);

    $this->mockMarketingCalculatorsApi()
        ->shouldReceive('calculateCatalog')
        ->once()
        ->withArgs(function (CalculateCatalogRequest $request) use ($offerWithCalc, $offerWithBadCalc) {
            assertEqualsCanonicalizing([$offerWithCalc->offer_id, $offerWithBadCalc->offer_id], Arr::pluck($request->getOffers(), 'offer_id'));

            return true;
        })
        ->andReturn(
            CalculateCatalogResponseFactory::new()->make(['offers' => [$offer]])
        );
    resolve(CalculateDiscountsAction::class)
        ->execute(DatabaseCollection::make([$offerWithPrice, $offerWithCalc, $offerWithBadCalc]));

    assertDatabaseHas(Offer::class, ['offer_id' => $offerWithCalc->offer_id, 'price' => $offerWithCalcPrice]);

    assertDatabaseHas(Offer::class, ['offer_id' => $offerWithBadCalc->offer_id, 'price' => $offerWithBadCalc->price]);
    assertDatabaseHas(Offer::class, ['offer_id' => $offerWithPrice->offer_id, 'price' => $offerWithPrice->price]);

    assertDatabaseHas(Discount::class, ['offer_id' => $offerWithCalc->offer_id, 'value' => $discountValue]);

    assertDatabaseMissing(Discount::class, ['offer_id' => $offerWithBadCalc->offer_id]);
    assertDatabaseHas(Discount::class, ['id' => $discountOfferWithPrice->id]);
})->with(FakerProvider::$optionalDataset);
