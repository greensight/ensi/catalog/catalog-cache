<?php

namespace App\Domain\Common\Actions;

use App\Domain\Offers\Actions\MigrateEntitiesFromCatalogAction;
use App\Domain\Offers\Actions\MigrateEntitiesFromCmsAction;
use App\Exceptions\ExceptionFormatter;
use Psr\Log\LoggerInterface;
use Throwable;

class MigrateEntitiesAction
{
    protected LoggerInterface $logger;

    public function __construct(
        public MigrateEntitiesFromCatalogAction $catalogAction,
        public MigrateEntitiesFromCmsAction $cmsAction,
    ) {
        $this->logger = logger()->channel('entities:migration');
    }

    public function execute(): void
    {
        try {
            $this->logger->info('Migrating data from Catalog PIM and Offers');
            $this->catalogAction->execute();
            $this->logger->info('Migrating data from CMS');
            $this->cmsAction->execute();
        } catch (Throwable $e) {
            $this->logger->error(ExceptionFormatter::line('Error occurred while migrating data', $e));
        }
    }
}
