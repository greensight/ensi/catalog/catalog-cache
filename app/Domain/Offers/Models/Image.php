<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ImageFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $image_id Image ID from PIM
 *
 * @property int $product_id Product ID from PIM
 * @property string|null $name Image description
 * @property int $sort Sorting order
 * @property string|null $file Image file
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class Image extends Model
{
    public $table = 'images';

    public static function factory(): ImageFactory
    {
        return ImageFactory::new();
    }
}
