<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ProductPropertyValueFactory;
use Carbon\CarbonInterface;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 *
 * @property int $product_property_value_id Product property value ID from PIM
 *
 * @property int $product_id Product ID from PIM
 * @property int $property_id Property ID from PIM
 * @property int|null $directory_value_id Directory value ID from PIM
 * @property string $type Value type
 * @property string $value Value
 * @property string|null $name Value name
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property Property|null $property
 * @property PropertyDirectoryValue|null $directoryValue
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class ProductPropertyValue extends Model
{
    protected $table = 'product_property_values';

    public static function factory(): ProductPropertyValueFactory
    {
        return ProductPropertyValueFactory::new();
    }

    public function directoryValue(): BelongsTo
    {
        return $this->belongsTo(PropertyDirectoryValue::class, 'directory_value_id', 'directory_value_id');
    }

    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class, 'property_id', 'property_id');
    }

    public function isFile(): bool
    {
        return in_array($this->type, [PropertyTypeEnum::IMAGE, PropertyTypeEnum::FILE]);
    }

    public function getPropValueAndName(): array
    {
        if ($this->directory_value_id) {
            if (!$this->directoryValue) {
                return [];
            }
            $value = $this->directoryValue->value;
            $name = $this->directoryValue->name;
        } else {
            $value = $this->value;
            $name = $this->name;
        }
        if ($this->isFile()) {
            $value = EnsiFile::public($value)->getUrl();
        }

        return [$value, $name];
    }
}
