<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\NameplateFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property int $nameplate_id Nameplate ID from CMS
 *
 * @property string $name Nameplate name
 * @property string $code Nameplate code
 *
 * @property string $background_color Background color
 * @property string $text_color Text color
 *
 * @property bool $is_active Active
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Collection|NameplateProduct[] $productLinks Product links to a nameplate
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class Nameplate extends Model
{
    protected $table = 'nameplates';

    public function productLinks(): HasMany
    {
        return $this->hasMany(NameplateProduct::class, 'nameplate_id', 'nameplate_id');
    }

    public static function factory(): NameplateFactory
    {
        return NameplateFactory::new();
    }
}
