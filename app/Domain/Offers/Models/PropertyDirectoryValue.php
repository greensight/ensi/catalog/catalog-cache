<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\PropertyDirectoryValueFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $directory_value_id Directory value id from PIM
 *
 * @property int $property_id Property ID from PIM
 * @property string|null $name Value name
 * @property string|null $code Character code
 * @property string $value Value
 * @property string $type Value type
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class PropertyDirectoryValue extends Model
{
    protected $table = 'property_directory_values';

    protected $casts = [
        'property_id' => 'int',
    ];

    public static function factory(): PropertyDirectoryValueFactory
    {
        return PropertyDirectoryValueFactory::new();
    }
}
