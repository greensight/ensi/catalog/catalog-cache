<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ProductGroupFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property int $product_group_id Product gluing ID from PIM
 *
 * @property int $category_id Category ID from PIM
 * @property string $name Name
 * @property int|null $main_product_id Main product ID from PIM
 * @property bool $is_active Activity from PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Category $category Category
 * @property-read Product $mainProduct Main product
 * @property-read Collection|Product[] $products Products gluing
 * @property-read Collection|ProductGroupProduct[] $productLinks Product links to gluing
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class ProductGroup extends Model
{
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function mainProduct(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'main_product_id', 'product_id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'product_group_product',
            'product_group_id',
            'product_id',
            'product_group_id',
            'product_id'
        );
    }

    public function productLinks(): HasMany
    {
        return $this->hasMany(ProductGroupProduct::class, 'product_group_id', 'product_group_id');
    }

    public static function factory(): ProductGroupFactory
    {
        return ProductGroupFactory::new();
    }
}
