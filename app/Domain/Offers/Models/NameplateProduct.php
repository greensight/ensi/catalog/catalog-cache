<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\NameplateProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 *
 * @property int $nameplate_product_id Bundles of nameplate and product from CMS
 *
 * @property int $nameplate_id Nameplate ID from CMS
 * @property int $product_id Product ID from PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class NameplateProduct extends Model
{
    protected $table = 'nameplate_product';

    public static function factory(): NameplateProductFactory
    {
        return NameplateProductFactory::new();
    }

    public function nameplate(): BelongsTo
    {
        return $this->belongsTo(Nameplate::class, 'nameplate_id', 'nameplate_id');
    }
}
