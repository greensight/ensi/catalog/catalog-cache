<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Image;
use Ensi\LaravelTestFactories\BaseModelFactory;

class ImageFactory extends BaseModelFactory
{
    protected $model = Image::class;

    public function definition(): array
    {
        $logoFile = $this->faker->nullable()->filePath();

        return [
            'image_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->company,
            'sort' => $this->faker->randomNumber(),
            'file' => $logoFile,
        ];
    }
}
