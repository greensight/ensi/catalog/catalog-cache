<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Category;
use Ensi\LaravelTestFactories\BaseModelFactory;

class CategoryFactory extends BaseModelFactory
{
    protected $model = Category::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'name' => $this->faker->company,
            'code' => $this->faker->slug,
            'parent_id' => $this->faker->nullable()->modelId(),

            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }
}
