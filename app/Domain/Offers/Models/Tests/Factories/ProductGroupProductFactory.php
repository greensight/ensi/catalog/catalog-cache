<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\ProductGroupProduct;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<ProductGroupProduct>
 */
class ProductGroupProductFactory extends BaseModelFactory
{
    protected $model = ProductGroupProduct::class;

    public function definition(): array
    {
        return [
            'product_group_product_id' => $this->faker->modelId(),
            'product_group_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
        ];
    }

    public function fastCreate(int $productGroupId, int $productId): ProductGroupProduct
    {
        return $this->create([
            'product_group_id' => $productGroupId,
            'product_id' => $productId,
        ]);
    }
}
