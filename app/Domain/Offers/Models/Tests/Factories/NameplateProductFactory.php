<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\NameplateProduct;
use Ensi\LaravelTestFactories\BaseModelFactory;

class NameplateProductFactory extends BaseModelFactory
{
    protected $model = NameplateProduct::class;

    public function definition(): array
    {
        return [
            'nameplate_product_id' => $this->faker->modelId(),
            'nameplate_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
        ];
    }
}
