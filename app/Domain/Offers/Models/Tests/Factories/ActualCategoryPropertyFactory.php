<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\ActualCategoryProperty;
use Ensi\LaravelTestFactories\BaseModelFactory;

class ActualCategoryPropertyFactory extends BaseModelFactory
{
    protected $model = ActualCategoryProperty::class;

    public function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'actual_category_property_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'is_gluing' => $this->faker->boolean(),

            'created_at' => $this->faker->date(),
            'updated_at' => $this->faker->date(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }
}
