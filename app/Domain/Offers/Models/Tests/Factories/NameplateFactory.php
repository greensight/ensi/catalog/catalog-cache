<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Nameplate;
use Ensi\LaravelTestFactories\BaseModelFactory;

class NameplateFactory extends BaseModelFactory
{
    protected $model = Nameplate::class;

    public function definition(): array
    {
        return [
            'nameplate_id' => $this->faker->modelId(),
            'name' => $this->faker->text(50),
            'code' => $this->faker->unique()->text(50),

            'background_color' => $this->faker->hexColor(),
            'text_color' => $this->faker->hexColor(),
        ];
    }
}
