<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\CategoryFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property int $category_id Category ID from PIM
 *
 * @property string $name Name
 * @property string $code CNC category code
 * @property int|null $parent_id ID of the parent category from PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Collection|ActualCategoryProperty[] $actualProperties Linking attributes to a category
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class Category extends Model
{
    protected $table = 'categories';

    public function actualProperties(): HasMany
    {
        return $this->hasMany(ActualCategoryProperty::class, 'category_id', 'category_id');
    }

    public static function factory(): CategoryFactory
    {
        return CategoryFactory::new();
    }
}
