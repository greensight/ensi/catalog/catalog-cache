<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ProductGroupProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 *
 * @property int $product_group_product_id ID of the product link and the product from PIM
 *
 * @property int $product_group_id Product gluing ID from PIM
 * @property int $product_id Product ID from PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read ProductGroup|null $productGroup Product gluing
 * @property-read Product $product Product
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class ProductGroupProduct extends Model
{
    protected $table = 'product_group_product';

    protected $casts = [
        'product_group_id' => 'int',
        'product_id' => 'int',
    ];

    public function productGroup(): BelongsTo
    {
        return $this->belongsTo(ProductGroup::class, 'product_group_id', 'product_group_id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }

    public static function factory(): ProductGroupProductFactory
    {
        return ProductGroupProductFactory::new();
    }
}
