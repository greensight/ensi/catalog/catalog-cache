<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\BrandFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $brand_id Brand ID from PIM
 *
 * @property string $name Name
 * @property string $code CNC code
 * @property string|null $description Brand description
 * @property string|null $logo_file Logotype file
 * @property string|null $logo_url Logotype URL
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class Brand extends Model
{
    protected $table = 'brands';

    public static function factory(): BrandFactory
    {
        return BrandFactory::new();
    }
}
