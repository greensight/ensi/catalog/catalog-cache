<?php

namespace App\Domain\Offers\Models;

use App\Domain\Discounts\Models\Discount;
use App\Domain\Offers\Models\Tests\Factories\OfferFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

/**
 * @property int $id
 *
 * @property int $offer_id Offer ID from Offers
 *
 * @property int $product_id Product ID from Pim
 *
 * @property bool $is_active The final activity of the offer
 *
 * @property int|null $base_price Base price
 * @property int|null $price The final discounted price. If null, it is calculated during indexing
 *
 * @property CarbonInterface|null $created_at Date of creation (in the current service, not an external one)
 * @property CarbonInterface|null $updated_at Update date (in the current service, not an external one)
 *
 * @property Product|null $product
 * @property Discount|null $discount
 * @property ProductGroup|null $productGroup
 * @property ProductGroupProduct|null $productGroupProduct
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class Offer extends Model
{
    protected $table = 'offers';

    public static function factory(): OfferFactory
    {
        return OfferFactory::new();
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }

    public function discount(): HasOne
    {
        return $this->hasOne(Discount::class, 'offer_id', 'offer_id');
    }

    public function productGroup(): HasOneThrough
    {
        // Product can be placed only in one product group at the time
        return $this->hasOneThrough(
            ProductGroup::class,
            ProductGroupProduct::class,
            'product_id',
            'product_group_id',
            'product_id',
            'product_group_id'
        );
    }

    public function productGroupProduct(): HasOne
    {
        // Product can be placed only in one product group at the time
        return $this->hasOne(ProductGroupProduct::class, 'product_id', 'product_id');
    }

    public function notMark(): void
    {
        $this->timestamps = false;
    }

    public static function queryForIndexing(): Builder
    {
        return Offer::query()
            ->with([
                'discount',
                'product.brand',
                'product.categories.actualProperties',
                'product.images',
                'product.productPropertyValues.property',
                'product.productPropertyValues.directoryValue',
                'product.productGroup.products.productPropertyValues.property',
                'product.productGroup.products.productPropertyValues.directoryValue',
                'product.productGroup.products.offers',
                'product.nameplates',
            ]);
    }
}
