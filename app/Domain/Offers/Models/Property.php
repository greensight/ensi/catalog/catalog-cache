<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\PropertyFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $property_id Property ID from PIM
 *
 * @property string $name The name on the showcase
 * @property string $code Property code
 * @property string $type Type
 * @property bool $is_public Display on the showcase
 * @property bool $is_active Property activity
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class Property extends Model
{
    protected $table = 'properties';

    public static function factory(): PropertyFactory
    {
        return PropertyFactory::new();
    }
}
