<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ActualCategoryPropertyFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 *
 * @property int $actual_category_property_id The connection ID from PIM
 *
 * @property int $property_id Property ID from PIM
 * @property int $category_id Category ID from PIM
 * @property bool $is_gluing The property is used for gluing products
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated Saved/created during migration of records from master services
 */
class ActualCategoryProperty extends Model
{
    protected $table = 'actual_category_properties';

    protected $casts = [
        'property_id' => 'int',
        'category_id' => 'int',
        'is_gluing' => 'bool',
    ];

    public static function factory(): ActualCategoryPropertyFactory
    {
        return ActualCategoryPropertyFactory::new();
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
