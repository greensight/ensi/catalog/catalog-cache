<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Elastic\Data\ElasticModel;
use App\Domain\Offers\Elastic\Data\Tests\Factories\OfferFactory;

/**
 * @property int $offer_id Offer ID from Offers
 * @property int $product_id Product ID from PIM
 * @property bool $allow_publish
 * @property string|null $main_image_file
 * @property int[] $category_ids Category ID from PIM
 * @property int|null $brand_id Brand ID from PIM
 * @property string $name Name for full-text search
 * @property string $name_sort Name for sorting
 * @property string $code Code to use in URL
 * @property string|null $description
 * @property int $type Product type from ProductTypeEnum
 * @property string $vendor_code Article
 * @property string|null $barcode EAN
 * @property float|null $weight Net weight in kg
 * @property float|null $weight_gross Gross weight in kg
 * @property float|null $width Width in mm
 * @property float|null $height Height in mm
 * @property float|null $length Length in mm
 * @property bool $is_adult Is product 18+
 * @property int|null $uom Unit of measurement from ProductUomEnum
 * @property int|null $tariffing_volume Unit of tariffication from ProductTariffingVolumeEnum
 * @property float|null $order_step
 * @property float|null $order_minvol Minimum quantity for order
 * @property int|null $cost Cost before discount
 * @property int|null $price Final price
 * @property DiscountData|null $discount
 * @property BrandData|null $brand
 * @property CategoryData[] $categories
 * @property ImageData[] $images
 * @property PropertyData[] $props Properties
 * @property string|null $gluing_name Product group name
 * @property bool|null $gluing_is_main Is main product of product group
 * @property bool $gluing_is_active Is product group active
 * @property GluingData[] $gluing Products from product group
 * @property NameplateData[] $nameplates Product tags
 */
class OfferData extends ElasticModel
{
    public function __construct($attributes = [])
    {
        $attributes['brand'] = isset($attributes['brand']) ? new BrandData($attributes['brand']) : null;
        $attributes['discount'] = isset($attributes['discount']) ? new DiscountData($attributes['discount']) : null;

        $categories = [];
        foreach ($attributes['categories'] ?? [] as $category) {
            $categories[] = new CategoryData($category);
        }
        $attributes['categories'] = $categories;

        $images = [];
        foreach ($attributes['images'] ?? [] as $image) {
            $images[] = new ImageData($image);
        }
        $attributes['images'] = $images;

        $properties = [];
        foreach ($attributes['props'] ?? [] as $property) {
            $properties[] = new PropertyData($property);
        }
        $attributes['props'] = $properties;

        $gluing = [];
        foreach ($attributes['gluing'] ?? [] as $data) {
            $gluing[] = new GluingData($data);
        }
        $attributes['gluing'] = $gluing;

        $nameplates = [];
        foreach ($attributes['nameplates'] ?? [] as $nameplate) {
            $nameplates[] = new NameplateData($nameplate);
        }
        $attributes['nameplates'] = $nameplates;

        parent::__construct($attributes);
    }

    public static function factory(): OfferFactory
    {
        return OfferFactory::new();
    }

    public function getId(): string|int
    {
        return $this->offer_id;
    }
}
