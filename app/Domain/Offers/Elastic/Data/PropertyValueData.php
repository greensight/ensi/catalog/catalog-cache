<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\PropertyValueFactory;
use Illuminate\Support\Fluent;

/**
 * @property string $value Значение
 * @property string|null $name Название значения
 */
class PropertyValueData extends Fluent
{
    public static function factory(): PropertyValueFactory
    {
        return PropertyValueFactory::new();
    }
}
