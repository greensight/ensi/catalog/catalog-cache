<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\GluingFactory;
use Illuminate\Support\Fluent;

/**
 * @property int $id ID товара
 * @property string $name Название товара
 * @property string $code ЧПУ код товара
 * @property GluingPropertyData[] $props Значения атрибутов склейки
 */
class GluingData extends Fluent
{
    public function __construct($attributes = [])
    {
        $props = [];
        foreach ($attributes['props'] ?? [] as $value) {
            $props[] = new GluingPropertyData($value);
        }
        $attributes['props'] = $props;

        parent::__construct($attributes);
    }

    public function addProp(GluingPropertyData $prop): void
    {
        $this->attributes['props'][] = $prop;
    }

    public static function factory(): GluingFactory
    {
        return GluingFactory::new();
    }
}
