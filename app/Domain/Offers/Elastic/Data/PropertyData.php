<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\PropertyFactory;
use Illuminate\Support\Fluent;

/**
 * @property string $type Тип
 * @property string $name Название на витрине
 * @property string $code Код атрибута
 * @property PropertyValueData[] $values Значения атрибута
 */
class PropertyData extends Fluent
{
    public function __construct($attributes = [])
    {
        $values = [];
        foreach ($attributes['values'] ?? [] as $value) {
            $values[] = new PropertyValueData($value);
        }
        $attributes['values'] = $values;

        parent::__construct($attributes);
    }

    public function addValue(PropertyValueData $value): void
    {
        $this->attributes['values'][] = $value;
    }

    public static function factory(): PropertyFactory
    {
        return PropertyFactory::new();
    }
}
