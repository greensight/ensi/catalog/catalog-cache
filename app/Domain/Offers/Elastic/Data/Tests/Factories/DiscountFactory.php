<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\DiscountData;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;

class DiscountFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'value_type' => $this->faker->randomElement(DiscountValueTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): DiscountData
    {
        return new DiscountData($this->makeArray($extra));
    }
}
