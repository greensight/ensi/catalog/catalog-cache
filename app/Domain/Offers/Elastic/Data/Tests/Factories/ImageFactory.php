<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\ImageData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ImageFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->company,
            'sort' => $this->faker->randomNumber(),
            'url' => $this->faker->imageUrl,
        ];
    }

    public function make(array $extra = []): ImageData
    {
        return new ImageData($this->makeArray($extra));
    }
}
