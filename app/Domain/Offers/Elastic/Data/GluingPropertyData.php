<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\GluingPropertyFactory;
use Illuminate\Support\Fluent;

/**
 * @property string $prop_type Тип атрибута
 * @property string $prop_name Название атрибута на витрине
 * @property string $prop_code Код атрибута
 * @property string $value_value Значение атрибута
 * @property string|null $value_name Название значения атрибута
 */
class GluingPropertyData extends Fluent
{
    public static function factory(): GluingPropertyFactory
    {
        return GluingPropertyFactory::new();
    }
}
