<?php

namespace App\Domain\Offers\Elastic\Tests\Factories;

use App\Domain\Offers\Elastic\OfferIndex;
use Ensi\LaravelTestFactories\Factory;

class OfferIndexFactory extends Factory
{
    protected function definition(): array
    {
        $stableIndexName = (new OfferIndex())->nameData()->toStringWithoutHash();

        return [
            'index' => $stableIndexName . '_' . $this->faker->ean8(),
            'creation.date' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
