<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Actions\MarkProductGroupOfferToIndexingAction;
use App\Domain\Offers\Models\ProductPropertyValue;
use App\Domain\Offers\Models\Property;
use Illuminate\Database\Eloquent\Builder;

class PropertyObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction,
        protected MarkProductGroupOfferToIndexingAction $markProductGroupAction,
    ) {
    }

    public function saved(Property $model): void
    {
        $this->mark($model);
        $this->markGroup($model);
    }

    protected function mark(Property $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'productPropertyValues',
                fn (Builder $queryPropertyValue) => $queryPropertyValue->where(
                    'property_id',
                    $model->property_id
                ),
            ),
        ));
    }

    protected function markGroup(Property $model): void
    {
        $pvT = (new ProductPropertyValue())->getTable();
        $this->markProductGroupAction->execute(
            filter: fn (Builder $queryProduct) =>
            $queryProduct->leftJoin($pvT, "{$pvT}.product_id", "=", "pgp2.product_id")
                ->where("{$pvT}.property_id", $model->property_id)
        );
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт удаление по ProductPropertyValue,
    // и через него будет пометка на обновление
}
