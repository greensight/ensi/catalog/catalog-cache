<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Actions\MarkProductGroupOfferToIndexingAction;
use App\Domain\Offers\Models\ProductPropertyValue;
use Illuminate\Database\Eloquent\Builder;

class ProductPropertyValueObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction,
        protected MarkProductGroupOfferToIndexingAction $markProductGroupAction,
    ) {
    }

    public function saved(ProductPropertyValue $model): void
    {
        $this->mark($model);
        $this->markGroup($model);
    }

    public function deleted(ProductPropertyValue $model): void
    {
        $this->markGroup($model);
        $this->mark($model);
    }

    protected function mark(ProductPropertyValue $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where('product_id', $model->product_id));
    }

    protected function markGroup(ProductPropertyValue $model): void
    {
        $this->markProductGroupAction->execute([$model->product_id]);
    }
}
