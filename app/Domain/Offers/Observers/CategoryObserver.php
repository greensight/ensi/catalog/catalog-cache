<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\Category;
use Illuminate\Database\Eloquent\Builder;

class CategoryObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(Category $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'categories',
                fn (Builder $queryCategory) => $queryCategory->where(
                    'categories.category_id',
                    $model->category_id
                ),
            ),
        ));
    }

    // Products are not marked for indexing on deleted,
    // because in this case, product update is expected to be received, which will trigger product indexing
}
