<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\ProductGroup;
use Illuminate\Database\Eloquent\Builder;

class ProductGroupObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction,
    ) {
    }

    public function saved(ProductGroup $model): void
    {
        $this->mark($model);
    }

    // При удалении не помечаем, т.к. придёт на удаление ещё ProductGroupProduct, и вот там пометятся все связанные товары

    protected function mark(ProductGroup $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'productGroupProduct',
            fn (Builder $queryProduct) => $queryProduct->where('product_group_id', $model->product_group_id),
        ));
    }
}
