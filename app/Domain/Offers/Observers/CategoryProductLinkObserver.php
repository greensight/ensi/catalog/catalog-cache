<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\CategoryProductLink;
use Illuminate\Database\Eloquent\Builder;

class CategoryProductLinkObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(CategoryProductLink $model): void
    {
        $this->mark($model);
    }

    public function deleted(CategoryProductLink $model): void
    {
        $this->mark($model);
    }

    protected function mark(CategoryProductLink $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where('product_id', $model->product_id));
    }
}
