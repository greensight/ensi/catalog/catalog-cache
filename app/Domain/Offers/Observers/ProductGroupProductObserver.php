<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\ProductGroupProduct;
use Illuminate\Database\Eloquent\Builder;

class ProductGroupProductObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(ProductGroupProduct $model): void
    {
        $this->mark($model);
    }

    public function deleted(ProductGroupProduct $model): void
    {
        $this->mark($model);
    }

    protected function mark(ProductGroupProduct $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where(function (Builder $query) use ($model) {
            $query
                // Для других товаров в этой связке
                ->whereHas(
                    'productGroupProduct',
                    fn (Builder $queryProduct) => $queryProduct->where('product_group_id', $model->product_group_id),
                )
                // Для текущего товара, т.к. ProductGroupProduct может быть уже удалён, первая проверка не выдаст текущий товар
                // Плюс включаем старый товар, если было изменение
                ->orWhereIn(
                    'product_id',
                    array_values(array_unique([$model->product_id, $model->getOriginal('product_id')]))
                );
        }));
    }
}
