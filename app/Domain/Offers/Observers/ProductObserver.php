<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Actions\MarkProductGroupOfferToIndexingAction;
use App\Domain\Offers\Models\Product;
use Illuminate\Database\Eloquent\Builder;

class ProductObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction,
        protected MarkProductGroupOfferToIndexingAction $markProductGroupAction,
    ) {
    }

    public function saved(Product $model): void
    {
        $this->mark($model);
        $this->markGroup($model);
    }

    public function deleted(Product $model): void
    {
        $this->mark($model);
        $this->markGroup($model);
    }

    protected function mark(Product $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where('product_id', $model->product_id));
    }

    protected function markGroup(Product $model): void
    {
        $this->markProductGroupAction->execute([$model->product_id]);
    }
}
