<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Actions\MarkProductGroupOfferToIndexingAction;
use App\Domain\Offers\Models\ProductPropertyValue;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use Illuminate\Database\Eloquent\Builder;

class PropertyDirectoryValueObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction,
        protected MarkProductGroupOfferToIndexingAction $markProductGroupAction,
    ) {
    }

    public function saved(PropertyDirectoryValue $model): void
    {
        $this->mark($model);
        $this->markGroup($model);
    }

    // На deleted не вешаем mark, т.к. в таком случае придёт обновление по ProductPropertyValue,
    // и через него будет пометка на обновление

    protected function mark(PropertyDirectoryValue $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'productPropertyValues',
                fn (Builder $queryPropertyValue) => $queryPropertyValue->where(
                    'directory_value_id',
                    $model->directory_value_id
                ),
            ),
        ));
    }

    protected function markGroup(PropertyDirectoryValue $model): void
    {
        $pvT = (new ProductPropertyValue())->getTable();
        $this->markProductGroupAction->execute(
            filter: fn (Builder $queryProduct) =>
            $queryProduct->leftJoin($pvT, "{$pvT}.product_id", "=", "pgp2.product_id")
                ->where("{$pvT}.directory_value_id", $model->directory_value_id)
        );
    }
}
