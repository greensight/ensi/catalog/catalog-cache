<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\DeleteOfferFromIndexAction;
use App\Domain\Offers\Actions\MarkProductGroupOfferToIndexingAction;
use App\Domain\Offers\Models\Offer;

class OfferObserver
{
    public function __construct(
        protected DeleteOfferFromIndexAction $deleteElasticAction,
        protected MarkProductGroupOfferToIndexingAction $markProductGroupAction,
    ) {
    }

    public function created(Offer $model): void
    {
        // На updated не нужно, т.к. ничего нужного от обновления не может измениться
        $this->mark($model);
    }

    public function deleted(Offer $model): void
    {
        $this->deleteElasticAction->execute($model->offer_id);
        $this->mark($model);
    }

    protected function mark(Offer $model): void
    {
        $this->markProductGroupAction->execute([$model->product_id]);
    }
}
