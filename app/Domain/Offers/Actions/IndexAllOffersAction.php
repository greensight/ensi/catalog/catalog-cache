<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Collection as CollectionDB;
use Illuminate\Support\Collection;

class IndexAllOffersAction
{
    public function __construct(private readonly IndexingOffersAction $indexOffers)
    {
    }

    public function execute(): void
    {
        Offer::queryForIndexing()
            ->chunkById(500, fn (Collection $offers) => $this->indexOffers->execute(CollectionDB::make($offers)));
    }
}
