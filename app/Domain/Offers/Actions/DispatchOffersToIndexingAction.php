<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Elastic\Actions\DispatchEntityToIndexingAction;
use App\Domain\Elastic\Contracts\DispatchEntityToIndexingInterface;
use App\Domain\Offers\Elastic\OfferIndex;
use App\Domain\Offers\Jobs\IndexingOffersJob;
use App\Domain\Offers\Models\Offer;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;

class DispatchOffersToIndexingAction implements DispatchEntityToIndexingInterface
{
    public function __construct(protected DispatchEntityToIndexingAction $dispatchEntityToIndexing)
    {
    }

    public function execute(): void
    {
        $this->dispatchEntityToIndexing->execute(
            index: new OfferIndex(),
            queryBuilder: function (?CarbonInterface $from, CarbonInterface $to) {
                /** @var Builder $query */
                $query = Offer::query()
                    ->where('updated_at', '<', $to)
                    ->selectRaw('offer_id as dispatch_id');

                if ($from) {
                    $query->where('updated_at', '>=', $from);
                }

                $query->orWhere(fn (Builder $q) => $q->whereNotNull('base_price')->whereNull('price'));

                return $query;
            },
            dispatch: function (array $ids) {
                IndexingOffersJob::dispatch($ids);
            },
            logger: logger()->channel('elastic:dispatch-to-indexing:offers')
        );
    }
}
