<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\ProductGroupProduct;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Expression;

class MarkProductGroupOfferToIndexingAction
{
    public function __construct(protected MarkOfferToIndexingAction $markAction)
    {
    }

    public function execute(?array $productIds = null, ?callable $filter = null): void
    {
        /**
         * select offer_id
         * from offers
         *     left join product_group_product as pgp1 on pgp1.product_id = offers.product_id
         *     left join product_group_product as pgp2 on pgp2.product_group_id = pgp1.product_group_id
         * where { pgp2.product_id IN (...) }
         */
        $this->markAction->execute(function (Builder $queryOffer) use ($productIds, $filter) {
            $pgpT = (new ProductGroupProduct())->getTable();
            $oT = (new Offer())->getTable();

            $queryOffer->leftJoin(new Expression("{$pgpT} as pgp1"), 'pgp1.product_id', '=', "{$oT}.product_id")
                ->leftJoin(new Expression("{$pgpT} as pgp2"), 'pgp2.product_group_id', '=', 'pgp1.product_group_id');

            if ($productIds) {
                $queryOffer->whereIn('pgp2.product_id', $productIds);
            }

            if ($filter) {
                $filter($queryOffer);
            }

        });
    }
}
