<?php

use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Domain\Offers\Actions\DispatchOffersToIndexingAction;
use App\Domain\Offers\Elastic\OfferIndex;
use App\Domain\Offers\Jobs\IndexingOffersJob;
use App\Domain\Offers\Models\Offer;
use Ensi\LaravelTestFactories\FakerProvider;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'DispatchOffersToIndexingAction');

test("Action DispatchOffersToIndexing success with IndexerTimestamp", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $index = new OfferIndex();
    /** @var IndexerTimestamp $timestamp */;
    $timestamp = IndexerTimestamp::factory()->forIndex($index)->create();

    // Офферы добавятся в джоб
    $offers = Offer::factory()->count(10)->create(['updated_at' => $timestamp->last_schedule->addDay()]);
    // Добавятся для обновления цены
    $offersWithoutPrice = Offer::factory()->count(2)
        ->create([
            'base_price' => 1000,
            'price' => null,
            'updated_at' => $timestamp->last_schedule->subDay(),
        ]);

    // Офферы не добавятся ни в один джоб
    Offer::factory()->count(10)->withPrice()->create(['updated_at' => $timestamp->last_schedule->subDay()]);

    Queue::fake();

    resolve(DispatchOffersToIndexingAction::class)->execute();

    Queue::assertPushed(function (IndexingOffersJob $job) use ($offers, $offersWithoutPrice) {
        return arrayEquals(
            $job->ids,
            array_merge($offers->pluck('offer_id')->all(), $offersWithoutPrice->pluck('offer_id')->all())
        );
    });
})->with(FakerProvider::$optionalDataset);

test("Action DispatchOffersToIndexing success without IndexerTimestamp", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */

    // Офферы добавятся в джоб
    $products = Offer::factory()->count(10)->create(['updated_at' => now()->subDay()]);

    Queue::fake();

    resolve(DispatchOffersToIndexingAction::class)->execute();

    Queue::assertPushed(function (IndexingOffersJob $job) use ($products) {
        return arrayEquals($job->ids, $products->pluck('offer_id')->all());
    });
})->with(FakerProvider::$optionalDataset);
