<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Offers\Actions\IndexingOffersAction;
use App\Domain\Offers\Models\Brand;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Nameplate;
use App\Domain\Offers\Models\NameplateProduct;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductPropertyValue;
use App\Domain\Offers\Models\Property;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use Ensi\LaravelElasticQuery\ElasticClient;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Database\Eloquent\Collection as DatabaseCollection;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('integration', 'IndexingOffersAction');

test("Action IndexingOffers success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ComponentTestCase $this */

    /** @var Property $property */
    $property = Property::factory()->create();
    /** @var PropertyDirectoryValue $directoryValue */
    $directoryValue = PropertyDirectoryValue::factory()->make(['property_id' => $property->property_id]);

    /** @var Brand $brand */
    $brand = Brand::factory()->create();
    /** @var Category $category */
    $category = Category::factory()->create();

    /** @var Product $product */
    $product = Product::factory()->inCategories([$category])->create([
        'brand_id' => $brand->brand_id,
    ]);

    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();
    NameplateProduct::factory()->create([
        'nameplate_id' => $nameplate->nameplate_id,
        'product_id' => $product->product_id,
    ]);
    $product->setRelation('nameplates', [$nameplate]);

    $productPropertyValue1 = ProductPropertyValue::factory()->simple()->create([
        'property_id' => $property->property_id,
        'product_id' => $product->product_id,
    ]);
    $productPropertyValue1->setRelation('property', $property);
    $productPropertyValue2 = ProductPropertyValue::factory()->directoryValueId($directoryValue->directory_value_id)->create([
        'property_id' => $property->property_id,
        'product_id' => $product->product_id,
    ]);
    $productPropertyValue2->setRelation('property', $property)
        ->setRelation('directoryValue', $directoryValue);
    $product->setRelation('productPropertyValues', [$productPropertyValue1, $productPropertyValue2]);

    /** @var Offer $offer */
    $offer = Offer::factory()->create([
        'product_id' => $product->product_id,
        'price' => 10_00,
    ]);
    $offer->setRelation('product', $product);

    Discount::factory()->for($offer)->create();

    $this->mock(ElasticClient::class)
        ->shouldReceive('bulk')
        ->times(1)
        ->andReturn(['items' => []]);

    resolve(IndexingOffersAction::class)->execute(DatabaseCollection::make([$offer]));
})->with(FakerProvider::$optionalDataset);
