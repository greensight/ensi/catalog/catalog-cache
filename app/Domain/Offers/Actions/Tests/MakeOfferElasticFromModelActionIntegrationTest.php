<?php

use App\Domain\Offers\Actions\MakeOfferElasticFromModelAction;
use App\Domain\Offers\Models\ActualCategoryProperty;
use App\Domain\Offers\Models\Brand;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use Illuminate\Testing\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('integration', 'MakeOfferElasticFromModelAction');

test("Action MakeOfferElasticFromModelAction success", function () {
    /** @var ComponentTestCase $this */

    /** @var Brand $brand */
    $brand = Brand::factory()->create();
    /** @var Category $category */
    $category = Category::factory()->create();

    ActualCategoryProperty::factory()
        ->create(['category_id' => $category->category_id, 'is_gluing' => true]);

    /** @var Product $product */
    $product = Product::factory()
        ->inCategories([$category])
        ->create(['brand_id' => $brand->brand_id]);

    /** @var Product $linkActiveProduct */
    $linkActiveProduct = Product::factory()
        ->inCategories([$category])
        ->create(['allow_publish' => true]);
    Offer::factory()->create(['product_id' => $linkActiveProduct->product_id]);
    /** @var Product $linkNotActiveProduct */
    $linkNotActiveProduct = Product::factory()
        ->inCategories([$category])
        ->create(['allow_publish' => false]);
    Offer::factory()->create(['product_id' => $linkNotActiveProduct->product_id]);

    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()
        ->create(['category_id' => $category->category_id, 'main_product_id' => $product->product_id]);

    ProductGroupProduct::factory()->fastCreate($productGroup->product_group_id, $product->product_id);
    ProductGroupProduct::factory()->fastCreate($productGroup->product_group_id, $linkActiveProduct->product_id);
    ProductGroupProduct::factory()->fastCreate($productGroup->product_group_id, $linkNotActiveProduct->product_id);

    /** @var Offer $offer */
    $offer = Offer::factory()->create([
        'product_id' => $product->product_id,
        'price' => 10_00,
    ]);

    /** @var Offer $offer */
    $offer = Offer::queryForIndexing()->findOrFail($offer->id);

    $offerData = resolve(MakeOfferElasticFromModelAction::class)->execute($offer);

    Assert::assertNotNull($offerData);
    Assert::assertEquals(1, count($offerData->gluing));
});
