<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Discounts\Actions\CalculateDiscountsAction;
use App\Domain\Elastic\Actions\SaveBulkAction;
use App\Domain\Offers\Elastic\OfferIndex;
use App\Domain\Offers\Models\Offer;
use App\Exceptions\ExceptionFormatter;
use Exception;
use Illuminate\Database\Eloquent\Collection as CollectionDB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;
use Throwable;

class IndexingOffersAction
{
    protected LoggerInterface $logger;

    public function __construct(
        protected SaveBulkAction $saveBulkAction,
        protected MakeOfferElasticFromModelAction $makeOfferElasticFromModelAction,
        private readonly CalculateDiscountsAction $calculateDiscounts,
    ) {
        $this->logger = logger()->channel('elastic:indexing:offers');
    }

    /**
     * @param Collection<Offer> $offers
     * @return void
     */
    public function execute(Collection $offers): void
    {
        $offers = CollectionDB::make($offers);

        try {
            $this->calculateDiscounts->execute($offers);
        } catch (Exception $e) {
            Log::channel('offers:discounts')
                ->critical("Ошибка обновления скидок у офферов", [
                    'offer_id' => $offers->pluck('offer_id')->all(),
                    'message' => $e->getMessage(),
                ]);
        }

        $indexOffers = [];
        $loggerInfo = [];
        $this->logger->info("=== Подготовка офферов");
        foreach ($offers as $offer) {
            $this->logger->info("Оффер: {$offer->id}. Начинаю подготовку индексации");

            try {
                $offerData = $this->makeOfferElasticFromModelAction->execute($offer);
                if (!$offerData) {
                    $this->logger->info("Оффер: {$offer->id}. Пропущен");

                    continue;
                }

                $loggerInfo[] = $offer->offer_id;
                $indexOffers[] = ["index" => ["_id" => $offer->offer_id]];
                $indexOffers[] = $offerData->toJson();

                $this->logger->info("Оффер: {$offer->id}. Добавлен в пакет для индексации");
            } catch (Throwable $e) {
                $this->logger->error("Оффер: {$offer->id}. " . ExceptionFormatter::line("Ошибка при подготовке к индексации", $e));

                continue;
            }
        }

        $index = new OfferIndex();
        $this->saveBulkAction->execute(
            index: $index,
            logger: $this->logger,
            bulk: $indexOffers,
            logContext: [
                'offerIds' => $loggerInfo,
            ]
        );
    }
}
