<?php

namespace App\Http\ApiV1\Support\Pagination\Elastic;

use App\Http\ApiV1\Support\Pagination\AbstractPageBuilder;
use Ensi\LaravelElasticQuerySpecification\SearchQueryBuilder;
use Illuminate\Http\Request;

abstract class AbstractElasticPageBuilder extends AbstractPageBuilder
{
    public function __construct(protected SearchQueryBuilder $query, protected Request $request)
    {
    }
}
