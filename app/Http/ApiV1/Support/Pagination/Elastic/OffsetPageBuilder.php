<?php

namespace App\Http\ApiV1\Support\Pagination\Elastic;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Pagination\Page;
use Illuminate\Database\Eloquent\Collection;

class OffsetPageBuilder extends AbstractElasticPageBuilder
{
    public function build(): Page
    {
        $limit = $this->applyMaxLimit((int) $this->request->input('pagination.limit', $this->getDefaultLimit()));

        return $limit > 0
            ? $this->buildWithPositiveLimit($limit)
            : $this->buildWithNotPositiveLimit($limit);
    }

    protected function buildWithNotPositiveLimit(int $limit): Page
    {
        $hits = ($limit < 0 && !$this->forbidToBypassPagination) ? $this->query->get() : new Collection();

        return new Page($hits, [
            'offset' => 0,
            'limit' => $limit,
            'total' => $hits->count(),
            'type' => PaginationTypeEnum::OFFSET->value,
        ]);
    }

    protected function buildWithPositiveLimit(int $limit): Page
    {
        $skip = (int) $this->request->input('pagination.offset', 0);

        $page = $this->query->paginate($limit, $skip);

        return new Page($page->hits, [
            'offset' => $skip,
            'limit' => $limit,
            'total' => $page->total,
            'type' => PaginationTypeEnum::OFFSET->value,
        ]);
    }
}
