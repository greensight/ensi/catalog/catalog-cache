<?php

namespace App\Http\ApiV1\Modules\Properties\Resources;

use App\Domain\Offers\Models\PropertyDirectoryValue;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin PropertyDirectoryValue
 */
class PropertyDirectoryValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'directory_value_id' => $this->directory_value_id,
            'property_id' => $this->property_id,
            'name' => $this->name,
            'code' => $this->code,
            'value' => $this->value,
            'type' => $this->type,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
