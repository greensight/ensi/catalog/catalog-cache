<?php

namespace App\Http\ApiV1\Modules\Properties\Resources;

use App\Domain\Offers\Models\Property;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Property
 */
class PropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'property_id' => $this->property_id,
            'name' => $this->name,
            'code' => $this->code,
            'type' => $this->type,
            'is_public' => $this->is_public,
            'is_active' => $this->is_active,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
