<?php

namespace App\Http\ApiV1\Modules\Properties\Queries;

use App\Domain\Offers\Models\Property;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PropertiesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Property::query());

        $this->allowedSorts(['id', 'name', 'code', 'type', 'is_public', 'is_active', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('property_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('is_public'),
            AllowedFilter::exact('is_active'),
            AllowedFilter::exact('is_migrated'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
