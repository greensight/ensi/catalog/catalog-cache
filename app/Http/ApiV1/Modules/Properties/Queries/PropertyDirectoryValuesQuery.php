<?php

namespace App\Http\ApiV1\Modules\Properties\Queries;

use App\Domain\Offers\Models\PropertyDirectoryValue;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PropertyDirectoryValuesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(PropertyDirectoryValue::query());

        $this->allowedSorts(['id', 'name', 'code', 'value', 'type', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('directory_value_id'),
            AllowedFilter::exact('property_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('value'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('is_migrated'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
