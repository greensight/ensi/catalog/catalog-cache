<?php

namespace App\Http\ApiV1\Modules\Properties\Controllers;

use App\Http\ApiV1\Modules\Properties\Queries\PropertiesQuery;
use App\Http\ApiV1\Modules\Properties\Resources\PropertiesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class PropertiesController
{
    public function search(PropertiesQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return PropertiesResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
