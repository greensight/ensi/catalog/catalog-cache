<?php

namespace App\Http\ApiV1\Modules\Properties\Controllers;

use App\Http\ApiV1\Modules\Properties\Queries\PropertyDirectoryValuesQuery;
use App\Http\ApiV1\Modules\Properties\Resources\PropertyDirectoryValuesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class PropertyDirectoryValuesController
{
    public function search(PropertyDirectoryValuesQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return PropertyDirectoryValuesResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
