<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountsResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductGroupProductsResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductGroupsResource;
use App\Http\ApiV1\Modules\Products\Resources\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Offer
 */
class OffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'offer_id' => $this->offer_id,
            'product_id' => $this->product_id,
            'base_price' => $this->base_price,
            'is_active' => $this->is_active,
            'price' => $this->price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_migrated' => $this->is_migrated,

            'product' => ProductsResource::make($this->whenLoaded('product')),
            'discount' => DiscountsResource::make($this->whenLoaded('discount')),
            'product_group' => ProductGroupsResource::make($this->whenLoaded('productGroup')),
            'product_group_product' => ProductGroupProductsResource::make($this->whenLoaded('productGroupProduct')),
        ];
    }
}
