<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'offers');

test('POST /api/v1/offers/offers:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Offer::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/offers/offers:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/offers/offers:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Offer $model */
    $model = Offer::factory()->create($value !== null ? [$fieldKey => $value] : []);

    postJson('/api/v1/offers/offers:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['offer_id'],
    ['product_id'],
    ['base_price'],
    ['is_active'],
    ['price'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/offers/offers:search sort success", function (string $sort) {
    Offer::factory()->create();

    postJson("/api/v1/offers/offers:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'base_price',
    'is_active',
    'price',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/offers/offers:search include success", function () {
    /** @var Offer $model */
    $model = Offer::factory()
        ->for(Product::factory(), 'product')
        ->create();

    Discount::factory()->create([
        'offer_id' => $model->offer_id,
    ]);

    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create([
        'main_product_id' => $model->product_id,
    ]);
    ProductGroupProduct::factory()->create([
        'product_id' => $model->product_id,
        'product_group_id' => $productGroup->product_group_id,
    ]);

    postJson("/api/v1/offers/offers:search", [
        "include" => [
            'product',
            'discount',
            'product_group',
            'product_group_product',
        ],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.product.id', $model->product->id)
        ->assertJsonPath('data.0.discount.id', $model->discount->id)
        ->assertJsonPath('data.0.product_group.id', $model->productGroup->id)
        ->assertJsonPath('data.0.product_group_product.id', $model->productGroupProduct->id);
});
