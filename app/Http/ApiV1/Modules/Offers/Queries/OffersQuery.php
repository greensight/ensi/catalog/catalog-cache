<?php

namespace App\Http\ApiV1\Modules\Offers\Queries;

use App\Domain\Offers\Models\Offer;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class OffersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Offer::query());

        $this->allowedIncludes(
            'product',
            'discount',
            AllowedInclude::relationship('product_group', 'productGroup'),
            AllowedInclude::relationship('product_group_product', 'productGroupProduct')
        );
        $this->allowedSorts(['id', 'base_price', 'is_active', 'price', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('offer_id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('base_price'),
            AllowedFilter::exact('is_active'),
            AllowedFilter::exact('price'),
            AllowedFilter::exact('is_migrated'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
