<?php

namespace App\Http\ApiV1\Modules\Offers\Controllers;

use App\Http\ApiV1\Modules\Offers\Queries\OffersQuery;
use App\Http\ApiV1\Modules\Offers\Resources\OffersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class OffersController
{
    public function search(OffersQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return OffersResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
