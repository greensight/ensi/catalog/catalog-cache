<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Offers\Models\Image;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ImagesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Image::query());

        $this->allowedSorts(['id', 'name', 'sort', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('image_id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::partial('name'),
            AllowedFilter::partial('file'),
            AllowedFilter::exact('sort'),
            AllowedFilter::exact('is_migrated'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
