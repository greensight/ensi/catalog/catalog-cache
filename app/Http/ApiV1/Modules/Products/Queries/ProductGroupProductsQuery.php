<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Offers\Models\ProductGroupProduct;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ProductGroupProductsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductGroupProduct::query());

        $this->allowedIncludes([
            'product',
            AllowedInclude::relationship('product_group', 'productGroup'),
        ]);
        $this->allowedSorts(['id', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_group_product_id'),
            AllowedFilter::exact('product_group_id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('is_migrated'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
