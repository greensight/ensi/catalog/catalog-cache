<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Offers\Models\Product;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\NumericFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ProductsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Product::query());

        $this->allowedIncludes([
            'brand',
            'categories',
            'images',
            'nameplates',
            'offers',
            AllowedInclude::relationship('product_property_values', 'productPropertyValues'),
            AllowedInclude::relationship('product_group', 'productGroup'),
        ]);
        $this->allowedSorts(['id', 'name', 'code', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('category_id', 'categories.category_id'),
            AllowedFilter::exact('brand_id'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('code'),
            AllowedFilter::partial('barcode'),
            AllowedFilter::partial('vendor_code'),
            AllowedFilter::exact('allow_publish'),
            AllowedFilter::exact('is_adult'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('is_migrated'),
            ...NumericFilter::make('weight')->lte()->gte(),
            ...NumericFilter::make('weight_gross')->lte()->gte(),
            ...NumericFilter::make('width')->lte()->gte(),
            ...NumericFilter::make('height')->lte()->gte(),
            ...NumericFilter::make('length')->lte()->gte(),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
