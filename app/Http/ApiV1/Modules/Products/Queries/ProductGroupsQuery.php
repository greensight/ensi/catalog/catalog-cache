<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Offers\Models\ProductGroup;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ProductGroupsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductGroup::query());

        $this->allowedIncludes([
            'category',
            'products',
            AllowedInclude::relationship('main_product', 'mainProduct'),
            AllowedInclude::relationship('product_links', 'productLinks'),
        ]);
        $this->allowedSorts(['id', 'name', 'is_active', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_group_id'),
            AllowedFilter::exact('category_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('main_product_id'),
            AllowedFilter::exact('is_active'),
            AllowedFilter::exact('is_migrated'),
            AllowedFilter::exact('is_active'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
