<?php

namespace App\Http\ApiV1\Modules\Products\Queries;

use App\Domain\Offers\Models\ProductPropertyValue;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class ProductPropertyValuesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductPropertyValue::query());
        $this->allowedIncludes([
            'property',
            AllowedInclude::relationship('directory_value', 'directoryValue'),
        ]);

        $this->allowedSorts(['id', 'type', 'value', 'name', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('product_property_value_id'),
            AllowedFilter::exact('product_id'),
            AllowedFilter::exact('property_id'),
            AllowedFilter::exact('directory_value_id'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('value'),
            AllowedFilter::partial('name'),
            AllowedFilter::exact('is_migrated'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
