<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Http\ApiV1\Modules\Products\Queries\ProductPropertyValuesQuery;
use App\Http\ApiV1\Modules\Products\Resources\ProductPropertyValuesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ProductPropertyValuesController
{
    public function search(ProductPropertyValuesQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return ProductPropertyValuesResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
