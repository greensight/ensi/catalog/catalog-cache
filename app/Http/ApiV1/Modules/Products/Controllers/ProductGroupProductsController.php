<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Http\ApiV1\Modules\Products\Queries\ProductGroupProductsQuery;
use App\Http\ApiV1\Modules\Products\Resources\ProductGroupProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ProductGroupProductsController
{
    public function search(ProductGroupProductsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return ProductGroupProductsResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
