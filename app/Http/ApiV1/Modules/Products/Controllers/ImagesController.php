<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Http\ApiV1\Modules\Products\Queries\ImagesQuery;
use App\Http\ApiV1\Modules\Products\Resources\ImagesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ImagesController
{
    public function search(ImagesQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return ImagesResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
