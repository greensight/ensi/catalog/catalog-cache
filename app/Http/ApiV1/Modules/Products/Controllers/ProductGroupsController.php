<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Http\ApiV1\Modules\Products\Queries\ProductGroupsQuery;
use App\Http\ApiV1\Modules\Products\Resources\ProductGroupsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ProductGroupsController
{
    public function search(ProductGroupsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return ProductGroupsResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
