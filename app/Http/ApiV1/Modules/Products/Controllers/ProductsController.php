<?php

namespace App\Http\ApiV1\Modules\Products\Controllers;

use App\Http\ApiV1\Modules\Products\Queries\ProductsQuery;
use App\Http\ApiV1\Modules\Products\Resources\ProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class ProductsController
{
    public function search(ProductsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return ProductsResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
