<?php

use App\Domain\Offers\Models\Brand;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Image;
use App\Domain\Offers\Models\Nameplate;
use App\Domain\Offers\Models\NameplateProduct;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Domain\Offers\Models\ProductPropertyValue;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'products');

test('POST /api/v1/products/products:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Product::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/products/products:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/products/products:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Product $model */
    $model = Product::factory()->create($value !== null ? [$fieldKey => $value] : []);

    postJson('/api/v1/products/products:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['brand_id'],
    ['name'],
    ['barcode'],
    ['vendor_code'],
    ['allow_publish'],
    ['is_adult'],
    ['type'],
    ['is_migrated'],
    ['weight', 10.0, 'weight_gte', 3.2],
    ['weight', 10.0, 'weight_lte', 13.2],
    ['weight_gross', 10.0, 'weight_gross_gte', 3.2],
    ['weight_gross', 10.0, 'weight_gross_lte', 13.2],
    ['width', 10.0, 'width_gte', 3.2],
    ['width', 10.0, 'width_lte', 13.2],
    ['height', 10.0, 'height_gte', 3.2],
    ['height', 10.0, 'height_lte', 13.2],
    ['length', 10.0, 'length_gte', 3.2],
    ['length', 10.0, 'length_lte', 13.2],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test('POST /api/v1/products/products:search filter by category success', function () {
    /** @var Category $category */
    $category = Category::factory()->create();

    /** @var Product $product */
    $product = Product::factory()->inCategories([$category])->create();
    Product::factory()->create();

    postJson('/api/v1/products/products:search', ['filter' => [
        'category_id' => $category->category_id,
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $product->id);
});

test("POST /api/v1/products/products:search sort success", function (string $sort) {
    Product::factory()->create();

    postJson("/api/v1/products/products:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'name',
    'code',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/products/products:search include success", function () {
    /** @var Category $category */
    $category = Category::factory()->create();

    /** @var Product $model */
    $model = Product::factory()
        ->for(Brand::factory(), 'brand')
        ->inCategories([$category])
        ->create();

    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create([
        'main_product_id' => $model->product_id,
    ]);
    ProductGroupProduct::factory()->create([
        'product_id' => $model->product_id,
        'product_group_id' => $productGroup->product_group_id,
    ]);

    $images = Image::factory()->count(3)->create([
        'product_id' => $model->product_id,
    ]);

    $productPropertyValues = ProductPropertyValue::factory()->count(3)->create([
        'product_id' => $model->product_id,
    ]);

    $nameplateProducts = NameplateProduct::factory()->count(3)->for(Nameplate::factory(), 'nameplate')->create([
        'product_id' => $model->product_id,
    ]);

    $offers = Offer::factory()->count(3)->create([
        'product_id' => $model->product_id,
    ]);

    postJson("/api/v1/products/products:search", [
        "include" => [
            'brand',
            'categories',
            'images',
            'nameplates',
            'offers',
            'product_property_values',
            'product_group',
        ],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.brand.id', $model->brand->id)
        ->assertJsonPath('data.0.categories.0.id', $model->categories->first()->id)
        ->assertJsonCount($images->count(), 'data.0.images')
        ->assertJsonCount($productPropertyValues->count(), 'data.0.product_property_values')
        ->assertJsonPath('data.0.product_group.id', $model->productGroup->id)
        ->assertJsonCount($nameplateProducts->count(), 'data.0.nameplates')
        ->assertJsonCount($offers->count(), 'data.0.offers');
});
