<?php

use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'products');

test('POST /api/v1/products/product-group-products:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = ProductGroupProduct::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/products/product-group-products:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/products/product-group-products:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var ProductGroupProduct $model */
    $model = ProductGroupProduct::factory()->create($value !== null ? [$fieldKey => $value] : []);

    postJson('/api/v1/products/product-group-products:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['product_group_product_id'],
    ['product_group_id'],
    ['product_id'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/products/product-group-products:search sort success", function (string $sort) {
    ProductGroupProduct::factory()->create();

    postJson("/api/v1/products/product-group-products:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/products/product-group-products:search include success", function () {
    /** @var ProductGroupProduct $model */
    $model = ProductGroupProduct::factory()
        ->for(ProductGroup::factory(), 'productGroup')
        ->for(Product::factory(), 'product')
        ->create();

    postJson("/api/v1/products/product-group-products:search", ["include" => ['product_group', 'product']])
        ->assertStatus(200)
        ->assertJsonPath('data.0.product_group.id', $model->productGroup->id)
        ->assertJsonPath('data.0.product.id', $model->product->id);
});
