<?php

use App\Domain\Offers\Models\ProductPropertyValue;
use App\Domain\Offers\Models\Property;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'products');

test('POST /api/v1/products/product-property-values:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = ProductPropertyValue::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/products/product-property-values:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/products/product-property-values:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var ProductPropertyValue $model */
    $model = ProductPropertyValue::factory()->create($value !== null ? [$fieldKey => $value] : []);

    postJson('/api/v1/products/product-property-values:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['product_property_value_id'],
    ['product_id'],
    ['property_id'],
    ['directory_value_id'],
    ['type'],
    ['value'],
    ['name'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/products/product-property-values:search sort success", function (string $sort) {
    ProductPropertyValue::factory()->create();

    postJson("/api/v1/products/product-property-values:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'type',
    'value',
    'name',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/products/product-property-values:search include success", function () {
    /** @var ProductPropertyValue $model */
    $model = ProductPropertyValue::factory()
        ->for(Property::factory(), 'property')
        ->for(PropertyDirectoryValue::factory(), 'directoryValue')
        ->create();

    postJson("/api/v1/products/product-property-values:search", ["include" => ['property', 'directory_value']])
        ->assertStatus(200)
        ->assertJsonPath('data.0.property.id', $model->property->id)
        ->assertJsonPath('data.0.directory_value.id', $model->directoryValue->id);
});
