<?php

use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'product-groups');

test('POST /api/v1/products/product-groups:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = ProductGroup::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/products/product-groups:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/products/product-groups:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var ProductGroup $model */
    $model = ProductGroup::factory()->create($value !== null ? [$fieldKey => $value] : []);

    postJson('/api/v1/products/product-groups:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['product_group_id'],
    ['category_id'],
    ['name'],
    ['main_product_id'],
    ['is_active'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/products/product-groups:search sort success", function (string $sort) {
    ProductGroup::factory()->create();

    postJson("/api/v1/products/product-groups:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'name',
    'is_active',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/products/product-groups:search include success", function () {
    /** @var ProductGroup $model */
    $model = ProductGroup::factory()
        ->for(Category::factory(), 'category')
        ->for(Product::factory(), 'mainProduct')
        ->create();
    $products = Product::factory()->count(3)->create();

    $productLinks = $products->map(function ($product) use ($model) {
        /** @var Product $product */
        return ProductGroupProduct::factory()->create([
            'product_group_id' => $model->product_group_id,
            'product_id' => $product->product_id,
        ]);
    });

    postJson("/api/v1/products/product-groups:search", ["include" => ['category', 'main_product', 'products', 'product_links']])
        ->assertStatus(200)
        ->assertJsonPath('data.0.category.id', $model->category->id)
        ->assertJsonPath('data.0.main_product.id', $model->mainProduct->id)
        ->assertJsonCount($products->count(), 'data.0.products')
        ->assertJsonCount($productLinks->count(), 'data.0.product_links');
});
