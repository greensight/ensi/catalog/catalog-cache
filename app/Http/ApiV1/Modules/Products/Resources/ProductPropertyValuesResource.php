<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Offers\Models\ProductPropertyValue;
use App\Http\ApiV1\Modules\Properties\Resources\PropertiesResource;
use App\Http\ApiV1\Modules\Properties\Resources\PropertyDirectoryValuesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductPropertyValue
 */
class ProductPropertyValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_property_value_id' => $this->product_property_value_id,
            'product_id' => $this->product_id,
            'property_id' => $this->property_id,
            'directory_value_id' => $this->directory_value_id,
            'type' => $this->type,
            'value' => $this->value,
            'name' => $this->name,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'property' => PropertiesResource::make($this->whenLoaded('property')),
            'directory_value' => PropertyDirectoryValuesResource::make($this->whenLoaded('directoryValue')),
        ];
    }
}
