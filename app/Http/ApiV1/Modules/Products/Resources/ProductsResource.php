<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Offers\Models\Product;
use App\Http\ApiV1\Modules\Brands\Resources\BrandsResource;
use App\Http\ApiV1\Modules\Categories\Resources\CategoriesResource;
use App\Http\ApiV1\Modules\Nameplates\Resources\NameplatesResource;
use App\Http\ApiV1\Modules\Offers\Resources\OffersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Product
 */
class ProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'allow_publish' => $this->allow_publish,
            'main_image_file' => $this->main_image_file,
            'category_ids' => $this->getCategoryIds(),
            'brand_id' => $this->brand_id,
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'type' => $this->type,
            'vendor_code' => $this->vendor_code,
            'barcode' => $this->barcode,
            'weight' => $this->weight,
            'weight_gross' => $this->weight_gross,
            'width' => $this->width,
            'height' => $this->height,
            'length' => $this->length,
            'is_adult' => $this->is_adult,
            'uom' => $this->uom,
            'tariffing_volume' => $this->tariffing_volume,
            'order_step' => $this->order_step,
            'order_minvol' => $this->order_minvol,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'brand' => BrandsResource::make($this->whenLoaded('brand')),
            'categories' => CategoriesResource::collection($this->whenLoaded('categories')),
            'images' => ImagesResource::collection($this->whenLoaded('images')),
            'product_property_values' => ProductPropertyValuesResource::collection($this->whenLoaded('productPropertyValues')),
            'nameplates' => NameplatesResource::collection($this->whenLoaded('nameplates')),
            'product_group' => ProductGroupsResource::make($this->whenLoaded('productGroup')),
            'offers' => OffersResource::collection($this->whenLoaded('offers')),
        ];
    }
}
