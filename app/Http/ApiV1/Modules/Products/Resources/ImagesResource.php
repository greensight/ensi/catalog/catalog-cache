<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Offers\Models\Image;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Image
 */
class ImagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'image_id' => $this->image_id,
            'product_id' => $this->product_id,
            'name' => $this->name,
            'sort' => $this->sort,
            'file' => $this->file,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
