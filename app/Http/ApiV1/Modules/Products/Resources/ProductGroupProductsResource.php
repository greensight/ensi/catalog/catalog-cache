<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Offers\Models\ProductGroupProduct;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductGroupProduct
 */
class ProductGroupProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_group_product_id' => $this->product_group_product_id,
            'product_group_id' => $this->product_group_id,
            'product_id' => $this->product_id,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'product_group' => ProductGroupsResource::make($this->whenLoaded('productGroup')),
            'product' => ProductsResource::make($this->whenLoaded('product')),
        ];
    }
}
