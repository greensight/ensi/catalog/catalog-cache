<?php

namespace App\Http\ApiV1\Modules\Products\Resources;

use App\Domain\Offers\Models\ProductGroup;
use App\Http\ApiV1\Modules\Categories\Resources\CategoriesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ProductGroup
 */
class ProductGroupsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'product_group_id' => $this->product_group_id,
            'category_id' => $this->category_id,
            'name' => $this->name,
            'main_product_id' => $this->main_product_id,
            'is_active' => $this->is_active,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'category' => CategoriesResource::make($this->whenLoaded('category')),
            'main_product' => ProductsResource::make($this->whenLoaded('mainProduct')),
            'products' => ProductsResource::collection($this->whenLoaded('products')),
            'product_links' => ProductGroupProductsResource::collection($this->whenLoaded('productLinks')),
        ];
    }
}
