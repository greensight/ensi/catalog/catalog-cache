<?php

namespace App\Http\ApiV1\Modules\Nameplates\Controllers;

use App\Http\ApiV1\Modules\Nameplates\Queries\NameplateProductsQuery;
use App\Http\ApiV1\Modules\Nameplates\Resources\NameplateProductsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class NameplateProductsController
{
    public function search(NameplateProductsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return NameplateProductsResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
