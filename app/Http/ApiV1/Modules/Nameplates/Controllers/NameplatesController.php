<?php

namespace App\Http\ApiV1\Modules\Nameplates\Controllers;

use App\Http\ApiV1\Modules\Nameplates\Queries\NameplatesQuery;
use App\Http\ApiV1\Modules\Nameplates\Resources\NameplatesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class NameplatesController
{
    public function search(NameplatesQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return NameplatesResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
