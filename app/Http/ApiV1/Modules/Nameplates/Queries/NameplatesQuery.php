<?php

namespace App\Http\ApiV1\Modules\Nameplates\Queries;

use App\Domain\Offers\Models\Nameplate;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class NameplatesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Nameplate::query());

        $this->allowedIncludes(['productLinks']);
        $this->allowedSorts(['id', 'name', 'code', 'background_color', 'text_color', 'is_active', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('nameplate_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('background_color'),
            AllowedFilter::exact('text_color'),
            AllowedFilter::exact('is_migrated'),
            AllowedFilter::exact('is_active'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
