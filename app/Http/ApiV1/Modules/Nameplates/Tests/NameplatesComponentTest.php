<?php

use App\Domain\Offers\Models\Nameplate;
use App\Domain\Offers\Models\NameplateProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'nameplates');

test('POST /api/v1/nameplates/nameplates:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Nameplate::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/nameplates/nameplates:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/nameplates/nameplates:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Nameplate $model */
    $model = Nameplate::factory()->create($value !== null ? [$fieldKey => $value] : []);

    postJson('/api/v1/nameplates/nameplates:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['name'],
    ['code'],
    ['background_color'],
    ['text_color'],
    ['is_active'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/nameplates/nameplates:search sort success", function (string $sort) {
    Nameplate::factory()->create();

    postJson("/api/v1/nameplates/nameplates:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'name',
    'code',
    'background_color',
    'text_color',
    'is_active',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/nameplates/nameplates:search include success", function () {
    /** @var Nameplate $model */
    $model = Nameplate::factory()->create();
    $products = NameplateProduct::factory()->for($model)->count(3)->create(['nameplate_id' => $model->nameplate_id]);

    postJson("/api/v1/nameplates/nameplates:search", ["include" => ['productLinks']])
        ->assertStatus(200)
        ->assertJsonCount($products->count(), 'data.0.product_links');
});
