<?php

use App\Domain\Offers\Models\NameplateProduct;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'nameplates');

test('POST /api/v1/nameplates/nameplate-products:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = NameplateProduct::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/nameplates/nameplate-products:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/nameplates/nameplate-products:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var NameplateProduct $model */
    $model = NameplateProduct::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/nameplates/nameplate-products:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['nameplate_product_id'],
    ['nameplate_id'],
    ['product_id'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/nameplates/nameplate-products:search", function (string $sort) {
    NameplateProduct::factory()->create();

    postJson("/api/v1/nameplates/nameplate-products:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'is_migrated',
    'updated_at',
    'created_at',
]);
