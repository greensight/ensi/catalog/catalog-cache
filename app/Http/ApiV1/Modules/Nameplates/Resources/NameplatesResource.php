<?php

namespace App\Http\ApiV1\Modules\Nameplates\Resources;

use App\Domain\Offers\Models\Nameplate;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Nameplate
 */
class NameplatesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'nameplate_id' => $this->nameplate_id,
            'name' => $this->name,
            'code' => $this->code,
            'background_color' => $this->background_color,
            'text_color' => $this->text_color,
            'is_active' => $this->is_active,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'product_links' => NameplateProductsResource::collection($this->whenLoaded('productLinks')),
        ];
    }
}
