<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\Modules\Offers\Resources\OffersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Discount
 */
class DiscountsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'offer_id' => $this->offer_id,
            'value_type' => $this->value_type,
            'value' => $this->value,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'offer' => OffersResource::make($this->whenLoaded('offer')),
        ];
    }
}
