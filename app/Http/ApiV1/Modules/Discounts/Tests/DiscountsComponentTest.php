<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Offers\Models\Offer;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'offers');

test('POST /api/v1/discounts/discounts:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Discount::factory()
        ->count(5)
        ->for(Offer::factory(), 'offer')
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/discounts/discounts:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/discounts/discounts:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Discount $model */
    $model = Discount::factory()->for(Offer::factory(), 'offer')->create($value !== null ? [$fieldKey => $value] : []);

    postJson('/api/v1/discounts/discounts:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['offer_id'],
    ['value_type'],
    ['value'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/discounts/discounts:search sort success", function (string $sort) {
    Discount::factory()->for(Offer::factory(), 'offer')->create();

    postJson("/api/v1/discounts/discounts:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'value_type',
    'value',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/discounts/discounts:search include success", function () {
    /** @var Discount $model */
    $model = Discount::factory()
        ->for(Offer::factory(), 'offer')
        ->create();

    postJson("/api/v1/discounts/discounts:search", [
        "include" => [
            'offer',
        ],
    ])
        ->assertStatus(200)
        ->assertJsonPath('data.0.offer.id', $model->offer->id);
});
