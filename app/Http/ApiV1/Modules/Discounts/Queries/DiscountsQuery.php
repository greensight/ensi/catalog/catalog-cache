<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\Discount;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DiscountsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Discount::query());

        $this->allowedIncludes('offer');
        $this->allowedSorts(['id', 'value_type', 'value', 'created_at', 'updated_at']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('offer_id'),
            AllowedFilter::exact('value_type'),
            AllowedFilter::exact('value'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
