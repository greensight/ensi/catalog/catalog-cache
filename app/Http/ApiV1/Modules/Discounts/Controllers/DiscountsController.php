<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Http\ApiV1\Modules\Discounts\Queries\DiscountsQuery;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class DiscountsController
{
    public function search(DiscountsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return DiscountsResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
