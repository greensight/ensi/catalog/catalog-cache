<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Offers\Models\ActualCategoryProperty;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ActualCategoryPropertiesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ActualCategoryProperty::query());

        $this->allowedSorts(['id', 'actual_category_property_id', 'property_id', 'category_id', 'is_gluing', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('actual_category_property_id'),
            AllowedFilter::exact('property_id'),
            AllowedFilter::exact('category_id'),
            AllowedFilter::exact('is_gluing'),
            AllowedFilter::exact('is_migrated'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
