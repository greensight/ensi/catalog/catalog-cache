<?php

namespace App\Http\ApiV1\Modules\Categories\Queries;

use App\Domain\Offers\Models\Category;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CategoriesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Category::query());

        $this->allowedIncludes(['actualProperties']);
        $this->allowedSorts(['id', 'category_id', 'name', 'code', 'parent_id', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('category_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('parent_id'),
            AllowedFilter::exact('is_migrated'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
