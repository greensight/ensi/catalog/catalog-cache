<?php

use App\Domain\Offers\Models\ActualCategoryProperty;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'categories');

test('POST /api/v1/categories/actual-properties:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = ActualCategoryProperty::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/categories/actual-properties:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/categories/actual-properties:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var ActualCategoryProperty $model */
    $model = ActualCategoryProperty::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/categories/actual-properties:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['actual_category_property_id'],
    ['category_id'],
    ['property_id'],
    ['is_gluing'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/categories/actual-properties:search sort success", function (string $sort) {
    ActualCategoryProperty::factory()->create();

    postJson("/api/v1/categories/actual-properties:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'actual_category_property_id',
    'property_id',
    'category_id',
    'is_gluing',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test('POST /api/v1/categories/actual-properties:search 400', function () {
    ActualCategoryProperty::factory()
        ->count(10)
        ->create();

    postJson('/api/v1/categories/actual-properties:search', [
        "filter" => ["test" => true],
    ])->assertStatus(400);
});
