<?php

use App\Domain\Offers\Models\ActualCategoryProperty;
use App\Domain\Offers\Models\Category;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'categories');

test('POST /api/v1/categories/categories:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Category::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/categories/categories:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/categories/categories:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Category $model */
    $model = Category::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/categories/categories:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['name'],
    ['code'],
    ['category_id'],
    ['parent_id'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/categories/categories:search sort success", function (string $sort) {
    Category::factory()->create();

    postJson("/api/v1/categories/categories:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'name',
    'code',
    'category_id',
    'parent_id',
    'is_migrated',
    'updated_at',
    'created_at',
]);

test("POST /api/v1/categories/categories:search include success", function () {
    /** @var Category $model */
    $model = Category::factory()->create();
    $properties = ActualCategoryProperty::factory()->for($model)->count(3)->create(['category_id' => $model->category_id]);

    postJson("/api/v1/categories/categories:search", ["include" => ['actualProperties']])
        ->assertStatus(200)
        ->assertJsonCount($properties->count(), 'data.0.actual_properties');
});

test('POST /api/v1/categories/categories:search 400', function () {
    Category::factory()
        ->count(10)
        ->create();

    postJson('/api/v1/categories/categories:search', [
        "filter" => ["test" => true],
    ])->assertStatus(400);
});
