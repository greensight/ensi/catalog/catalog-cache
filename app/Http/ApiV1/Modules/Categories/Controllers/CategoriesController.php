<?php

namespace App\Http\ApiV1\Modules\Categories\Controllers;

use App\Http\ApiV1\Modules\Categories\Queries\CategoriesQuery;
use App\Http\ApiV1\Modules\Categories\Resources\CategoriesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class CategoriesController
{
    public function search(CategoriesQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return CategoriesResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
