<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Offers\Models\Category;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Category
 */
class CategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'name' => $this->name,
            'code' => $this->code,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_migrated' => $this->is_migrated,

            'actual_properties' => ActualCategoryPropertiesResource::collection($this->whenLoaded('actualProperties')),
        ];
    }
}
