<?php

namespace App\Http\ApiV1\Modules\Categories\Resources;

use App\Domain\Offers\Models\ActualCategoryProperty;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ActualCategoryProperty
 */
class ActualCategoryPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'actual_category_property_id' => $this->actual_category_property_id,
            'property_id' => $this->property_id,
            'category_id' => $this->category_id,
            'is_gluing' => $this->is_gluing,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_migrated' => $this->is_migrated,
        ];
    }
}
