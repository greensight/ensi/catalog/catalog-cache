<?php

namespace App\Http\ApiV1\Modules\Elastic\Queries;

use App\Domain\Elastic\Models\IndexerTimestamp;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\ExtraFilter;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class IndexerTimestampsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(IndexerTimestamp::query());

        $this->allowedSorts(['id', 'index', 'stage', 'index_hash', 'last_schedule']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('index'),
            AllowedFilter::exact('stage'),
            AllowedFilter::partial('index_hash'),
            ExtraFilter::predefined('is_current', fn (Builder $query) => $query->whereIsCurrent()),
            ...DateFilter::make('last_schedule')->lte()->gte(),
        ]);
    }
}
