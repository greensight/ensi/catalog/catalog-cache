<?php

namespace App\Http\ApiV1\Modules\Elastic\Resources;

use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Domain\Elastic\Utils\IndexConfChecker;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin IndexerTimestamp
 */
class IndexerTimestampsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $isCurrentIndex = IndexConfChecker::checkIndex($this->index, $this->index_hash);
        $isCurrentStage = IndexConfChecker::checkStage($this->stage);

        return [
            'id' => $this->id,
            'index' => $this->index,
            'stage' => $this->stage,
            'index_hash' => $this->index_hash,
            'last_schedule' => $this->last_schedule,

            'is_current_index' => $isCurrentIndex,
            'is_current_stage' => $isCurrentStage,
            'is_current' => $isCurrentIndex && $isCurrentStage,
        ];
    }
}
