<?php

namespace App\Http\ApiV1\Modules\Elastic\Controllers;

use App\Domain\Elastic\Actions\DeleteIndexerTimestampAction;
use App\Domain\Elastic\Actions\PatchIndexerTimestampAction;
use App\Http\ApiV1\Modules\Elastic\Queries\IndexerTimestampsQuery;
use App\Http\ApiV1\Modules\Elastic\Requests\PatchIndexerTimestampRequest;
use App\Http\ApiV1\Modules\Elastic\Resources\IndexerTimestampsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class IndexerTimestampsController
{
    public function search(IndexerTimestampsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return IndexerTimestampsResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }

    public function delete(int $id, DeleteIndexerTimestampAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function patch(int $id, PatchIndexerTimestampRequest $request, PatchIndexerTimestampAction $action): Responsable
    {
        return new IndexerTimestampsResource($action->execute($id, $request->validated()));
    }
}
