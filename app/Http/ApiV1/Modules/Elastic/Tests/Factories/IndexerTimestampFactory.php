<?php

namespace App\Http\ApiV1\Modules\Elastic\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LaravelTestFactories\BaseApiFactory;

class IndexerTimestampFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'last_schedule' => $this->faker->dateTime()->format(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
