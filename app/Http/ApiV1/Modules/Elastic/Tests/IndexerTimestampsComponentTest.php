<?php

use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Domain\Offers\Elastic\OfferIndex;
use App\Http\ApiV1\Modules\Elastic\Tests\Factories\IndexerTimestampFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'elastic');

test('POST /api/v1/elastic/indexer-timestamps:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = IndexerTimestamp::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/elastic/indexer-timestamps:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/elastic/indexer-timestamps:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var IndexerTimestamp $model */
    $model = IndexerTimestamp::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/elastic/indexer-timestamps:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['index'],
    ['stage'],
    ['index_hash'],
    ['last_schedule', '2022-04-20T01:32:08.000000Z', 'last_schedule_gte', '2022-04-19T01:32:08.000000Z'],
    ['last_schedule', '2022-04-20T01:32:08.000000Z', 'last_schedule_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("/api/v1/elastic/indexer-timestamps:search sort success", function (string $sort) {
    IndexerTimestamp::factory()->create();

    postJson("/api/v1/elastic/indexer-timestamps:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'index',
    'stage',
    'index_hash',
    'last_schedule',
]);

test("POST /api/v1/elastic/indexer-timestamps:search resource is_current success", function () {
    $index = new OfferIndex();
    IndexerTimestamp::factory()->forIndex($index)->create();

    postJson("/api/v1/elastic/indexer-timestamps:search")
        ->assertStatus(200)
        ->assertJsonPath("data.0.is_current_index", true)
        ->assertJsonPath("data.0.is_current_stage", true)
        ->assertJsonPath("data.0.is_current", true);
});

test("POST /api/v1/elastic/indexer-timestamps:search filter is_current success", function () {
    $index = new OfferIndex();
    /** @var IndexerTimestamp $model */
    $model = IndexerTimestamp::factory()->forIndex($index)->create();

    postJson("/api/v1/elastic/indexer-timestamps:search", [
        "filter" => ["is_current" => true],
    ])
        ->assertStatus(200)
        ->assertJsonPath("data.0.id", $model->id)
        ->assertJsonPath("data.0.is_current", true);
});

test("POST /api/v1/elastic/indexer-timestamps:search resource is_current_index success", function () {
    $index = new OfferIndex();
    IndexerTimestamp::factory()
        ->forIndex($index)
        ->state(fn (array $fields) => ['stage' => $fields['stage'] . '1'])
        ->create();

    $timestamps = postJson("/api/v1/elastic/indexer-timestamps:search")
        ->assertStatus(200)
        ->json('data');

    foreach ($timestamps as $timestamp) {
        assertEquals(true, $timestamp['is_current_index']);
        assertEquals(false, $timestamp['is_current_stage']);
        assertEquals(false, $timestamp['is_current']);
    }
});

test("POST /api/v1/elastic/indexer-timestamps:search resource is_current_stage success", function () {
    $index = new OfferIndex();
    IndexerTimestamp::factory()
        ->forIndex($index)
        ->state(fn (array $fields) => ['index' => $fields['index'] . '1'])
        ->create();

    $timestamps = postJson("/api/v1/elastic/indexer-timestamps:search")
        ->assertStatus(200)
        ->json('data');

    foreach ($timestamps as $timestamp) {
        assertEquals(false, $timestamp['is_current_index']);
        assertEquals(true, $timestamp['is_current_stage']);
        assertEquals(false, $timestamp['is_current']);
    }
});

test('POST /api/v1/elastic/indexer-timestamps:search 400', function () {
    IndexerTimestamp::factory()
        ->count(10)
        ->create();

    postJson('/api/v1/elastic/indexer-timestamps:search', [
        "filter" => ["test" => true],
    ])->assertStatus(400);
});

test('DELETE /api/v1/elastic/indexer-timestamps/{id} 200', function () {
    /** @var IndexerTimestamp $timestamp */
    $timestamp = IndexerTimestamp::factory()->create();
    $id = $timestamp->id;

    deleteJson("/api/v1/elastic/indexer-timestamps/{$id}")
        ->assertStatus(200);

    assertModelMissing($timestamp);
});

test('PATCH /api/v1/elastic/indexer-timestamps/{id} 200', function () {
    $timestamp = IndexerTimestamp::factory()->create();
    $id = $timestamp->id;
    $request = IndexerTimestampFactory::new()->make();

    patchJson("/api/v1/elastic/indexer-timestamps/{$id}", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.last_schedule', $request['last_schedule']);

    assertDatabaseHas($timestamp->getTable(), ['id' => $id, 'last_schedule' => $request['last_schedule']]);
});
