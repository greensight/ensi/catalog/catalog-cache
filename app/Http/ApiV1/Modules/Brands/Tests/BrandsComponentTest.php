<?php

use App\Domain\Offers\Models\Brand;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'brands');

test('POST /api/v1/brands/brands:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $models = Brand::factory()
        ->count(5)
        ->create();

    $filteredModels = $models->sortBy('id');

    postJson('/api/v1/brands/brands:search', [
        "sort" => ["-id"],
    ])
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $filteredModels->last()->id);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/brands/brands:search filter success', function (string $fieldKey, mixed $value = null, ?string $filterKey = null, mixed $filterValue = null) {
    /** @var Brand $model */
    $model = Brand::factory()->create($value ? [$fieldKey => $value] : []);

    postJson('/api/v1/brands/brands:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $model->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::CURSOR, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $model->id);
})->with([
    ['id'],
    ['name'],
    ['code'],
    ['description'],
    ['brand_id'],
    ['is_migrated'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['created_at', '2022-04-20T01:32:08.000000Z', 'created_at_lte', '2022-04-21T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_gte', '2022-04-19T01:32:08.000000Z'],
    ['updated_at', '2022-04-20T01:32:08.000000Z', 'updated_at_lte', '2022-04-21T01:32:08.000000Z'],
]);

test("POST /api/v1/brands/brands:search sort success", function (string $sort) {
    Brand::factory()->create();

    postJson("/api/v1/brands/brands:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'id',
    'name',
    'code',
    'description',
    'is_migrated',
    'updated_at',
    'created_at',
]);
