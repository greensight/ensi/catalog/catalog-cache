<?php

namespace App\Http\ApiV1\Modules\Brands\Queries;

use App\Domain\Offers\Models\Brand;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BrandsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Brand::query());

        $this->allowedSorts(['id', 'name', 'code', 'description', 'created_at', 'updated_at', 'is_migrated']);
        $this->defaultSort('id');

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('brand_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('description'),
            AllowedFilter::exact('is_migrated'),
            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);
    }
}
