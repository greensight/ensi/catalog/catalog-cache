<?php

namespace App\Http\ApiV1\Modules\Brands\Resources;

use App\Domain\Offers\Models\Brand;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Brand
 */
class BrandsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'brand_id' => $this->brand_id,
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'logo_file' => $this->logo_file,
            'logo_url' => $this->logo_url,
            'is_migrated' => $this->is_migrated,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
