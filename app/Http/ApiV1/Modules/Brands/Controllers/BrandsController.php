<?php

namespace App\Http\ApiV1\Modules\Brands\Controllers;

use App\Http\ApiV1\Modules\Brands\Queries\BrandsQuery;
use App\Http\ApiV1\Modules\Brands\Resources\BrandsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Contracts\Support\Responsable;

class BrandsController
{
    public function search(BrandsQuery $query, PageBuilderFactory $pageBuilderFactory): Responsable
    {
        return BrandsResource::collectPage(
            $pageBuilderFactory->fromEloquentQuery($query)->build()
        );
    }
}
