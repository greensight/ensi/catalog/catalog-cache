<?php

namespace App\Http\ApiV1\Modules\ElasticOffers\Resources;

use App\Domain\Offers\Elastic\Data\PropertyValueData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin PropertyValueData
 */
class PropertyValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'value' => $this->value,
            'name' => $this->name,
        ];
    }
}
