<?php

namespace App\Http\ApiV1\Modules\ElasticOffers\Queries;

use App\Domain\Offers\Elastic\OfferIndex;
use App\Domain\Offers\Elastic\OfferSpecification;
use Ensi\LaravelElasticQuerySpecification\QueryBuilderRequest;
use Ensi\LaravelElasticQuerySpecification\SearchQueryBuilder;

class OffersQuery extends SearchQueryBuilder
{
    public function __construct(QueryBuilderRequest $request)
    {
        parent::__construct(OfferIndex::query(), new OfferSpecification(), $request);
    }
}
