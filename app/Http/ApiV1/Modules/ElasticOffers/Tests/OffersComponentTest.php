<?php

use App\Domain\Offers\Elastic\Data\OfferData;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\PimClient\Dto\ProductTypeEnum;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

use Tests\ElasticTestTrait;

uses(ApiV1ComponentTestCase::class, ElasticTestTrait::class);
uses()->group('component');

test('POST /api/v1/elastic-offers/offers:search 200 filter success', function (?bool $always, string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    FakerProvider::$optionalAlways = $always;

    /** @var OfferData $offerData */
    $offerData = OfferData::factory()->create($value ? [$fieldKey => $value] : []);
    OfferData::factory()->create(is_bool($value) ? [$fieldKey => !$value] : ['type' => ProductTypeEnum::WEIGHT]);

    postJson('/api/v1/elastic-offers/offers:search', ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $offerData->{$fieldKey}),
    ], 'sort' => ['product_id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 10]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $offerData->offer_id);
})->with(FakerProvider::$optionalDataset, [
    ['offer_id'],
    ['product_id'],
    ['allow_publish', true],
    ['code'],
    ['vendor_code'],
    ['barcode', 'barcode'],
    ['is_adult', true],
    ['type', ProductTypeEnum::PACKED],
    ['brand_id'],
    ['gluing_is_main', true],
    ['gluing_is_active', true],
    ['name', 'name'],
    ['category_ids', [1], 'category_id', 1],
]);

test("POST /api/v1/elastic-offers/offers:search sort success", function (string $sort, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    OfferData::factory()->create();

    postJson("/api/v1/elastic-offers/offers:search", ["sort" => [$sort]])->assertStatus(200);
})->with([
    'product_id',
    'offer_id',
    'name_sort',
    'code',
], FakerProvider::$optionalDataset);

test('POST /api/v1/elastic-offers/offers:search 200 include success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $offers = [];
    for ($i = 0; $i < 10; $i++) {
        $offers[] = OfferData::factory()->withAllRelations()->create(['offer_id' => $i + 1]);
    }
    /** @var OfferData $offerData */
    $offerData = current($offers);

    postJson('/api/v1/elastic-offers/offers:search', [
        'include' => ['categories', 'brand', 'images', 'attributes', 'gluing', 'discount', 'nameplates'], 'sort' => ['offer_id'],
    ])
        ->assertStatus(200)
        ->assertJsonCount(count($offers), 'data')
        ->assertJsonPath('data.0.id', $offerData->offer_id)
        ->assertJsonPath('data.0.discount.value', $offerData->discount->value)
        ->assertJsonPath('data.0.discount.value_type', $offerData->discount->value_type)
        ->assertJsonPath('data.0.brand.id', $offerData->brand->id)
        ->assertJsonPath('data.0.categories.0.id', current($offerData->categories)->id)
        ->assertJsonPath('data.0.images.0.id', current($offerData->images)->id)
        ->assertJsonPath('data.0.attributes.0.code', current($offerData->props)->code)
        ->assertJsonPath('data.0.gluing.0.id', current($offerData->gluing)->id)
        ->assertJsonPath('data.0.nameplates.0.id', current($offerData->nameplates)->id);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/elastic-offers/offers/{id} 200', function () {
    FakerProvider::$optionalAlways = true;

    /** @var OfferData $offerData */
    $offerData = OfferData::factory()->withAllRelations()->create();

    getJson("/api/v1/elastic-offers/offers/$offerData->offer_id?include=categories,brand,images,attributes,gluing,discount,nameplates")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $offerData->offer_id)
        ->assertJsonPath('data.discount.value', $offerData->discount->value)
        ->assertJsonPath('data.discount.value_type', $offerData->discount->value_type)
        ->assertJsonPath('data.brand.id', $offerData->brand->id)
        ->assertJsonPath('data.categories.0.id', current($offerData->categories)->id)
        ->assertJsonPath('data.images.0.id', current($offerData->images)->id)
        ->assertJsonPath('data.attributes.0.code', current($offerData->props)->code)
        ->assertJsonPath('data.gluing.0.id', current($offerData->gluing)->id)
        ->assertJsonPath('data.nameplates.0.id', current($offerData->nameplates)->id);
})->with(FakerProvider::$optionalDataset);
