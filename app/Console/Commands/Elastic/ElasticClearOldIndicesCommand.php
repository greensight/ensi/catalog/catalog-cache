<?php

namespace App\Console\Commands\Elastic;

use App\Domain\Elastic\Actions\ClearOldIndexAction;
use Illuminate\Console\Command;

class ElasticClearOldIndicesCommand extends Command
{
    protected $signature = 'elastic:clear-old-indices {--days=} {--stages=} {--hashes=}';
    protected $description = 'Remove old indices';

    public function handle(ClearOldIndexAction $action): void
    {
        $days = (int)($this->option('days') ?: ClearOldIndexAction::DAYS);
        $datetime = now()->subDays($days)->startOfDay();

        $stages = $this->option('stages');
        $stages = $stages ? explode(',', $stages) : null;

        $hashes = $this->option('hashes');
        $hashes = $hashes ? explode(',', $hashes) : null;

        $action->execute(
            datetime: $datetime,
            stages: $stages,
            hashes: $hashes
        );
    }
}
