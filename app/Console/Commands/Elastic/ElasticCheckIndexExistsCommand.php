<?php

namespace App\Console\Commands\Elastic;

use App\Domain\Elastic\Actions\ApplyIndexAction;
use App\Domain\Elastic\Concerns\ElasticIndex;
use Illuminate\Console\Command;

class ElasticCheckIndexExistsCommand extends Command
{
    protected $signature = 'elastic:check-index-exists';
    protected $description = 'Check if up-to-date index exists';

    public function handle(ApplyIndexAction $applyIndexAction): int
    {
        $flag = true;

        $applyIndexAction->execute(function (ElasticIndex $index) use (&$flag) {
            if (!$index->isCreated()) {
                $flag = false;
                $this->output->warning("Index {$index->indexName()} not created");
            }
        });

        $this->output->info("All indices created");

        return $flag ? self::SUCCESS : self::FAILURE;
    }
}
