<?php

namespace App\Console\Commands\Elastic;

use App\Domain\Elastic\Actions\ElasticRecordProcessAction;
use Illuminate\Console\Command;

class ElasticRecordProcessCommand extends Command
{
    protected $signature = 'elastic:records-process';
    protected $description = 'Index records updated since last indexing';

    public function handle(ElasticRecordProcessAction $action): void
    {
        $action->execute();
    }
}
