<?php

namespace App\Console\Commands\Offers;

use App\Domain\Common\Actions\MigrateEntitiesAction;
use Illuminate\Console\Command;

class OffersMigrateFromApiCommand extends Command
{
    protected $signature = 'offers:migrate-from-api';
    protected $description = 'Start synchronization of data objects from master services';

    public function handle(MigrateEntitiesAction $action): void
    {
        $action->execute();
    }
}
