<?php

namespace App\Domain\Elastic\Tests;

use App\Console\Commands\Elastic\ElasticClearOldIndicesCommand;
use App\Console\Tests\Cases\ElasticClearOldIndicesIntegrationTestCase;
use App\Domain\Elastic\Actions\ClearOldIndexAction;
use App\Domain\Elastic\Data\ElasticIndexNameData;
use App\Domain\Elastic\Utils\IndexConfig;
use App\Domain\Offers\Elastic\OfferIndex;
use Carbon\Carbon;
use Ensi\LaravelElasticQuery\ElasticQuery;

use function Pest\Laravel\artisan;
use function Pest\Laravel\assertModelExists;
use function Pest\Laravel\assertModelMissing;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertFileDoesNotExist;
use function PHPUnit\Framework\assertFileExists;
use function PHPUnit\Framework\assertTrue;

use Tests\ElasticTestTrait;

uses(ElasticClearOldIndicesIntegrationTestCase::class, ElasticTestTrait::class);
uses()->group('component', 'commands', 'ElasticClearOldIndicesCommand');

test("Command ElasticClearOldIndicesCommand success", function () {
    /** @var ElasticClearOldIndicesIntegrationTestCase $this */

    // It is important not to mix the indices from other tests
    config()->set('app.name', config('app.name') . 'ElasticClearOldIndicesCommand');
    config()->set('elastic.synonym_local_config_folder', 'ElasticClearOldIndicesCommand');
    $old = now();
    $fresh = $old->addDays(ClearOldIndexAction::DAYS);

    $settings = [];
    $existIndices = $existTimestamps = $existSynonyms = [];
    $missingIndices = $missingTimestamps = $missingSynonyms = [];

    // Index with INDEXER
    $indexSafe = new ElasticIndexNameData(
        index: OfferIndex::build()->nameData()->index,
        hash: IndexConfig::hashFromArray($settings),
    );
    $existTimestamps[] = $this->createTimestamp($indexSafe, $fresh);
    $existIndices[] = $this->createIndex($indexSafe);
    // Index with other STAGE
    $indexOtherStage = clone $indexSafe;
    $indexOtherStage->stage .= '1';
    $missingTimestamps[] = $this->createTimestamp($indexOtherStage, $old);
    $missingIndices[] = $this->createIndex($indexOtherStage);
    // Index with other HASH
    $indexOtherHash = clone $indexSafe;
    $indexOtherHash->hash = IndexConfig::hashFromArray(array_merge($settings, ['other' => 'other']));
    $missingIndices[] = $this->createIndex($indexOtherHash);

    Carbon::setTestNow($old->addDays(ClearOldIndexAction::DAYS + 1));
    artisan(ElasticClearOldIndicesCommand::class);

    foreach ($missingIndices as $indexName) {
        assertFalse(ElasticQuery::indicesExists($indexName), "Index {$indexName} should not exist");
    }

    foreach ($missingTimestamps as $timestamp) {
        assertModelMissing($timestamp);
    }

    foreach ($existIndices as $indexName) {
        assertTrue(ElasticQuery::indicesExists($indexName), "Index {$indexName} must exist");
    }

    foreach ($existTimestamps as $timestamp) {
        assertModelExists($timestamp);
    }
});
