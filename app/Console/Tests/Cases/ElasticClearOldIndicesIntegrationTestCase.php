<?php

namespace App\Console\Tests\Cases;

use App\Domain\Elastic\Data\ElasticIndexNameData;
use App\Domain\Elastic\Models\IndexerTimestamp;
use Carbon\CarbonInterface;
use Ensi\LaravelElasticQuery\ElasticQuery;
use Tests\IntegrationTestCase;

class ElasticClearOldIndicesIntegrationTestCase extends IntegrationTestCase
{
    protected array $indices = [];

    protected function tearDown(): void
    {
        if ($this->indices) {
            $delete = [];
            foreach ($this->indices as $index) {
                if (ElasticQuery::indicesExists($index)) {
                    $delete[] = $index;
                }
            }
            if ($delete) {
                ElasticQuery::indicesDelete(join(',', $delete));
            }
        }

        parent::tearDown();
    }

    public function createTimestamp(ElasticIndexNameData $name, ?CarbonInterface $lastSchedule = null): IndexerTimestamp
    {
        return IndexerTimestamp::factory()->create([
            'index' => $name->index,
            'stage' => $name->stage,
            'index_hash' => $name->hash,
            'last_schedule' => $lastSchedule ?: now()->subMinute(),
        ]);
    }

    public function createIndex(ElasticIndexNameData $name): string
    {
        $indexName = $name->toString();
        if (!ElasticQuery::indicesExists($indexName)) {
            ElasticQuery::indicesCreate($indexName, []);
        }
        $this->indices[] = $indexName;

        return $indexName;
    }
}
