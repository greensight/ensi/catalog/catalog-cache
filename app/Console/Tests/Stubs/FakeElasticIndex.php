<?php

namespace App\Console\Tests\Stubs;

use App\Domain\Offers\Elastic\OfferIndex;

class FakeElasticIndex extends OfferIndex
{
    public function settings(): array
    {
        return ['tmp'];
    }
}
