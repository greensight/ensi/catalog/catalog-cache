SearchElasticOffersRequest:
  type: object
  properties:
    sort:
      $ref: '../../common_schemas.yaml#/RequestBodySort'
    filter:
      type: object
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'
    pagination:
      $ref: '../../common_schemas.yaml#/RequestBodyPagination'

SearchOneElasticOffersRequest:
  type: object
  properties:
    filter:
      type: object
    include:
      $ref: '../../common_schemas.yaml#/RequestBodyInclude'

ElasticOfferResponse:
  type: object
  properties:
    data:
      $ref: '#/ElasticOffer'
    meta:
      type: object
  required:
    - data

SearchElasticOffersResponse:
  type: object
  properties:
    data:
      type: array
      items:
        $ref: '#/ElasticOffer'
    meta:
      type: object
      properties:
        pagination:
          $ref: '../../common_schemas.yaml#/ResponseBodyPagination'
  required:
    - data
    - meta

ElasticOfferProperties:
  type: object
  properties:
    id:
      type: integer
      description: Offer ID from Offers
    product_id:
      type: integer
      description: Product ID from PIM
    allow_publish:
      type: boolean
    main_image_file:
      $ref: '../../common_schemas.yaml#/FileNullable'
    category_ids:
      type: array
      items:
        type: integer
      description: Category IDs from PIM
    brand_id:
      type: integer
      description: Brand ID from PIM
      nullable: true
    name:
      type: string
    code:
      type: string
      description: Code to use in URL
    description:
      type: string
      nullable: true
    type:
      type: integer
      description: Product type from ProductTypeEnum from PIM
    vendor_code:
      type: string
      description: Article
    barcode:
      type: string
      description: EAN
      nullable: true
    weight:
      type: number
      description: Net weight in kg
      nullable: true
    weight_gross:
      type: number
      description: Gross weight in kg
      nullable: true
    width:
      type: number
      description: Width in mm
      nullable: true
    height:
      type: number
      description: Height in mm
      nullable: true
    length:
      type: number
      description: Length in mm
      nullable: true
    is_adult:
      type: boolean
      description: Is product 18+
    uom:
      type: integer
      nullable: true
      description: Unit of measurement from ProductUomEnum from PIM
      example: 1
    tariffing_volume:
      type: integer
      nullable: true
      description: Unit of tariffication from ProductTariffingVolumeEnum from PIM
      example: 1
    order_step:
      type: number
      nullable: true
      example: 0.7
    order_minvol:
      type: number
      nullable: true
      description: Minimum quantity for order
      example: 1.4
    price:
      type: integer
      description: Final price
      nullable: true
    cost:
      type: integer
      description: Cost before discount
      nullable: true
    gluing_name:
      type: string
      description: Product group name
      nullable: true
    gluing_is_main:
      type: boolean
      description: Is main product of product group
      nullable: true
    gluing_is_active:
      type: boolean
      description: Is product group active
      nullable: true
      example: true
  required:
    - id
    - product_id
    - allow_publish
    - main_image_file
    - category_ids
    - brand_id
    - name
    - code
    - description
    - type
    - vendor_code
    - barcode
    - weight
    - weight_gross
    - width
    - height
    - length
    - is_adult
    - uom
    - tariffing_volume
    - order_step
    - order_minvol
    - price
    - cost
    - gluing_name
    - gluing_is_main
    - gluing_is_active

ElasticOfferIncludes:
  type: object
  properties:
    discount:
      $ref: './discounts.yaml#/ElasticDiscount'
    brand:
      $ref: './brands.yaml#/ElasticBrand'
    categories:
      type: array
      items:
        $ref: './categories.yaml#/ElasticCategory'
    images:
      type: array
      items:
        $ref: './images.yaml#/ElasticImage'
    attributes:
      type: array
      items:
        $ref: './attributes.yaml#/ElasticAttribute'
    gluing:
      type: array
      items:
        $ref: './gluing.yaml#/Gluing'
    nameplates:
      type: array
      items:
        $ref: './nameplates.yaml#/ElasticNameplate'

ElasticOffer:
  allOf:
    - $ref: '#/ElasticOfferProperties'
    - $ref: '#/ElasticOfferIncludes'
