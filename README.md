# Ensi Catalog Cache

## Main info

Name: Catalog Cache  
Domain: Catalog  
Purpose: Index management

## Development

Instructions describing how to deploy, launch and test the service on a local machine can be found in a separate document at [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

The regulations for working on tasks are also in [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

### ElasticSearch commands

`elastic:show` - Show info about current index

`elastic:check-index-exists` - Check if up-to-date index exists

`elastic:index-migrate` - Check if up-to-date index exists and create new, if it does not

`elastic:records-process` - Index records updated since last indexing

`elastic:clear-old-indices` - Remove old indices (only 2 newest are stored)

#### Accepted order of work for commands

1. Each time the service is shipped, the `elastic:index-migrate` command is executed, which (if the index is missing or its structure has changed) creates the index. 
2. The `elastic:records-process` command runs every minute and adds entities that have changed since the last run to the indexing queue (this is tracked via the `IndexerTimestamp` model. Since the start time is fixated for the unique index hash and APP_STAGE, multiple indexers can be run in parallel).

The `elastic:clear-old-indices` command is run once a day to remove old indexes.

The `elastic:check-index-exists` command blocks the loading of the web part of the service if the index does not exist.

#### Data filling

The main flow of data filling is through reading Kafka topics. The list of topics is described in the "Dependencies" - "Kafka" block. To read topic data, workers are launched on the stand that execute the command `php artisan kafka:consume <topic name>`.

For the initial filling of the database, the console command `offers:migrate-from-api` was implemented, which migrates data from master services with data.

The `offers:all-indexing` command indexes all available offers

## Service structure

You can read about the service structure [here](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Dependencies

| Name              | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Environment variables                                                                                                                          |
|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| PostgreSQL        | Service database                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD                                                          |
| Kafka             | Message broker. <br/> Consumer is listening to following topics:<br/> `<contour>.catalog.fact.offers.1`</br>`<contour>.catalog.fact.brands.1`</br>`<contour>.catalog.fact.categories.1`</br>`<contour>.catalog.fact.property-directory-values.1`</br>`<contour>.catalog.fact.properties.1`</br>`<contour>.catalog.fact.actual-category-properties.1`</br>`<contour>.catalog.fact.published-images.1`</br>`<contour>.catalog.fact.published-products.1`</br>`<contour>.catalog.fact.category-product-links.1`</br>`<contour>.catalog.fact.published-property-values.1`</br>`<contour>.catalog.fact.product-groups.1`</br>`<contour>.catalog.fact.product-group-products.1`</br>`<contour>.cms.fact.nameplates.1`</br>`<contour>.cms.fact.nameplate-product-links.1`</br>`<contour>.cms.fact.discount-catalog-calculate.1`</br> | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| Elasticsearch     | Search engine                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | ELASTICSEARCH_HOSTS                                                                                                                            |
| **Ensi services** | **Ensi services with which this service communicates**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| Catalog           | Ensi Offers<br/>Ensi PIM                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | CATALOG_OFFERS_SERVICE_HOST<br/>CATALOG_PIM_SERVICE_HOST                                                                                       |
| CMS               | Ensi Cms                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | CMS_CMS_SERVICE_HOST                                                                                                                           |
| Marketing         | Ensi Marketing                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | MARKETING_MARKETING_SERVICE_HOST                                                                                                               |

## Environments

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/catalog-cache/job/catalog-cache/  
URL: https://catalog-cache-master-dev.ensi.tech/docs/swagger

### Preprod

N/A

### Prod

N/A

## Contacts

The team supporting this service: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email: mail@greensight.ru

## License

[Open license for the right to use the Greensight Ecom Platform (GEP) computer program](LICENSE.md).
