<?php

namespace Tests;

use App\Domain\Elastic\Data\ElasticIndexNameData;
use Ensi\LaravelElasticQuery\ElasticQuery;
use Illuminate\Support\Facades\ParallelTesting;

trait ElasticTestTrait
{
    protected function setUpElasticTestTrait(): void
    {
        config()->set('app.stage', config('app.stage') . (ParallelTesting::token() ?: 0));

        // Before each test, we delete unnecessary indexes and set configuration
        $this->forgetIndices();
    }

    protected function forgetIndices(): void
    {
        $indices = ElasticQuery::catIndices(ElasticIndexNameData::toStringStageApp());
        if ($indices) {
            ElasticQuery::indicesDelete(join(',', array_map(fn (array $index) => $index['index'], $indices)));
        }
    }
}
