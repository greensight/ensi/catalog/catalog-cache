<?php

namespace Tests;

use Ensi\LaravelTestFactories\WithFakerProviderTestCase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFakerProviderTestCase;

    public function stage(string $name): self
    {
        config()->set('app.stage', $name);

        return $this;
    }
}
