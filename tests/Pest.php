<?php

use App\Domain\Elastic\Concerns\ElasticIndex;
use Illuminate\Database\Eloquent\Model;
use Mockery\MockInterface;

use function Pest\Laravel\mock;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertGreaterThan;

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

uses(Tests\TestCase::class)->in('Feature');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

expect()->extend('toBeOne', function () {
    return $this->toBe(1);
});

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

function mockAction(string $class, mixed $returnValue = null): MockInterface
{
    return mock($class)->allows(['execute' => $returnValue]);
}

function arrayEquals(array $ar1, array $ar2): bool
{
    sort($ar1);
    sort($ar2);

    return $ar1 == $ar2;
}

function assertNewModelFieldEquals(Model $model, string $field): void
{
    $newModel = $model::query()->where($model->getKeyName(), $model->getKey())->first();

    assertEquals($model->{$field}, $newModel->{$field});
}

function assertNewModelFieldGreaterThan(Model $model, string $field): void
{
    $newModel = $model::query()->where($model->getKeyName(), $model->getKey())->first();

    assertGreaterThan($model->{$field}, $newModel->{$field});
}

/*
|--------------------------------------------------------------------------
| ElasticSearch
|--------------------------------------------------------------------------
|
| Функции для тестирования эластика
|
*/

function assertElasticMissing(ElasticIndex $index, array $query): void
{
    // Это нужно, чтобы эластик сразу данные начинал отдавать в поиске, иначе у него внутренние задержки ломают тесты
    $index->indicesRefresh();

    $response = $index->search([
        'size' => 0,
        'from' => 0,
        'query' => $query,
        'track_total_hits' => true,
    ]);
    assertEquals(0, $response['hits']['total']['value']);
}
